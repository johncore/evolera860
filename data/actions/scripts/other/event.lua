-- Credits StreamSide and Empty
function onUse(cid, item, fromPosition, itemEx, toPosition)

	if getPlayerStorageValue(cid,11552) < 1 then
		if getPlayerLevel(cid) > 1 then
			getPlayerStorageValue(cid, 11552)
			doSendAnimatedText(getPlayerPosition(cid), "Welcome!", TEXTCOLOR_RED) 
			doCreatureSay(cid, "CONGRATULATIONS! You are now a Event Player for 30 days!. ", TALKTYPE_ORANGE_1)
			setPlayerStorageValue(cid, 11552, (getPlayerStorageValue(cid,11552) + 30))
			doRemoveItem(item.uid, 1)
		else
			doPlayerSendCancel(cid,"You need to be at least level 2 to use this.")
			doRemoveItem(item.uid, 1)
		end
	else
		doPlayerSendCancel(cid,"You are already a Event.")
	end	
return TRUE
end