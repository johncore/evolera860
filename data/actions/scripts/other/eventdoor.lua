-- Credits StreamSide and Empty
function onUse(cid, item, fromPosition, itemEx, toPosition)
	local cidPosition = getCreaturePosition(cid)
		if (item.actionid == 5789 and getPlayerStorageValue(cid,11552) >= 1) then
			if cidPosition.x < toPosition.x then
				doTeleportThing(cid, {x=toPosition.x+1,y=toPosition.y,z=toPosition.z}, TRUE)
								doCreatureSay(cid, "Welcome in Event Area!", TALKTYPE_ORANGE_1)
			else
				doTeleportThing(cid, {x=toPosition.x-1,y=toPosition.y,z=toPosition.z}, TRUE)
								doCreatureSay(cid, "Welcome in Event Area!", TALKTYPE_ORANGE_1)
			end
			return TRUE
		else
			doPlayerSendTextMessage(cid, MESSAGE_INFO_DESCR, "Sorry, but only Event Players Can Go there. Go And Loot Event Token")
			return TRUE
	end
	return FALSE
end
