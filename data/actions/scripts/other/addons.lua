local t = {
	[12001] = 'Citizen',
	[12002] = 'Hunter',
	[12003] = 'Mage',
	[12004] = 'Knight',
	[12005] = 'Nobleman',
	[12006] = 'Summoner',
	[12007] = 'Warrior',
	[12008] = 'Barbarian',
	[12009] = 'Druid',
	[12010] = 'Wizard',
	[12011] = 'Oriental',
	[12012] = 'Pirate',
	[12013] = 'Assassin',
	[12014] = 'Beggar',
	[12015] = 'Shaman',
	[12016] = 'Norse',
	[12017] = 'Nightmare',
	[12018] = 'Jester',
	[12019] = 'Brotherhood',
	[12020] = 'Demonhunter',
	[12021] = 'Yalaharian',
	[12022] = 'Warmaster'
}
 
function onUse(cid, item, fromPosition, itemEx, toPosition)
	if not canPlayerWearOutfitId(cid, item.uid - 12000, 3) then
		doCreatureSay(cid, 'You now have the ' ..t[item.uid] .. ' Addons!', TALKTYPE_ORANGE_1)
		doPlayerAddOutfitId(cid, item.uid - 12000, 3)
		doSendMagicEffect(getThingPos(cid), math.random(67))
	else
		doCreatureSay(cid, 'You already have the ' .. t[item.uid] .. ' Addons!', TALKTYPE_ORANGE_1, false, cid)
	end
	return true
end