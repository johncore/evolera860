local config =
{
	needQuestToUse = false, --true je�li potrzeba questa aby u�ywa�
	questStatus = 567890, -- warto�� storage questa, je�li wy�ej true
	showItem = 2148 --jaki item ma by� widocznym w otwieranej ksi��ce
}
function onUse(cid, item, fromPos, itemEx, toPos)
local monsterInfo, loot, childLoot = {}, {}, {}
local monster = itemEx.uid
local infos = 0
local text, lootText, childLootText = "", "", ""
	if(config.needQuestToUse == true) then
		if(getPlayerStorageValue(cid, config.questStatus) == -1) then
			doPlayerSendTextMessage(cid, 22, 'You need finish quest to use this item.')
			return true
		end
	end
	if(isMonster(monster) == false) then
		doPlayerSendTextMessage(cid, 22, 'You can only use it on monsters.')
		return true
	end
	infos = getMonsterInfo(getCreatureName(monster))
	loot = infos.loot
	childLoot = loot[#loot].child
	for k, v in ipairs(loot) do
		lootText = lootText..""..getItemNameById(v.id)..", chance: "..(tonumber(v.chance)/100)..'\n'
	end
	for k, v in ipairs(childLoot) do
		childLootText = childLootText..""..getItemNameById(v.id)..", chance: "..(tonumber(v.chance)/100)..'\n'
	end
	monsterInfo =
	{
		'                          '..infos.name..'\n',
		'                     Informations:\n\n',
		'Health points:   '..infos.healthMax..'\n',
		'Experience:      '..infos.experience..'\n',
		'Speed:                '..infos.baseSpeed..'\n',
		'Armor:                '..infos.armor..'\n',
		'Defense:            '..infos.defense..'\n\n',
		'              Special informations:\n\n',
		'Summonable:    '..convertBoolToString(infos.summonable)..'\n',
		'Illusionable:      '..convertBoolToString(infos.illusionable)..'\n',
		'Convinceable:  '..convertBoolToString(infos.convinceable)..'\n',
		'Summonable:     '..convertBoolToString(infos.summonable)..'\n\n',
		'                         Loot:\n\n'..lootText..'\n',
		'                     Loot in bag:\n\n'..childLootText
	}
	for k,v in ipairs(monsterInfo) do
		text = text..""..v
	end
	doShowTextDialog(cid, config.showItem, text)
return true
end
function convertBoolToString(bool)
	if(bool == true) then
		return "Yes"
	else
		return "No"
	end
end
