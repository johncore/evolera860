function onUse(cid, item, fromPosition, itemEx, toPosition)
local health = math.random(20000, 50000)
		if(item.itemid == 8635) and getGlobalStorageValue(53436) <= os.time() then
			doCreatureAddHealth(cid, health)
			doSendMagicEffect(getCreaturePosition(cid), 12)
			doCreatureSay(cid, "Ahhh...", TALKTYPE_ORANGE_1)
			setGlobalStorageValue(53435, os.time() + 60)
			doSendAnimatedText(getPlayerPosition(cid), health, 23)
		else
			doPlayerSendCancel(cid,"You cannot use this yet.")
			doSendMagicEffect(getCreaturePosition(cid), 2)
		end
	return TRUE
end