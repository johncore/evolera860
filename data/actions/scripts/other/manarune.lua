local exhaust = createConditionObject(CONDITION_EXHAUST)
setConditionParam(exhaust, CONDITION_PARAM_TICKS, (getConfigInfo('timeBetweenExActions') - 100))

function onUse(cid, item, fromPosition, itemEx, toPosition)
	local level = getPlayerLevel(cid)
	local mlevel = getPlayerMagLevel(cid)
	local mana_minimum = (level * 1) + (mlevel * 9) - 0
	local mana_maximum = (level * 3.5) + (mlevel * 15)
	local mana_add = math.random(mana_minimum, mana_maximum)
	
	doPlayerAddMana(cid, mana_add)
	doSendMagicEffect(getThingPos(itemEx.uid), CONST_ME_MAGIC_BLUE)
	doCreatureSay(itemEx.uid, "Mana UP!", TALKTYPE_ORANGE_1)
	return TRUE
end