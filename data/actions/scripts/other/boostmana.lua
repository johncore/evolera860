function onUse(cid, item, fromPosition, itemEx, toPosition)
local mana = math.random(2000, 3000)
		if(item.itemid == 8633) and getGlobalStorageValue(53437) <= os.time() then
			doPlayerAddMana(cid, mana)
			doSendMagicEffect(getCreaturePosition(cid), 12)
			doCreatureSay(cid, "Ahhh...", TALKTYPE_ORANGE_1)
			setGlobalStorageValue(53433, os.time() + 60)
			doSendAnimatedText(getPlayerPosition(cid), mana, 23)
		else
			doPlayerSendCancel(cid,"You cannot use this yet.")
			doSendMagicEffect(getCreaturePosition(cid), 2)
		end
	return TRUE
end