local config = {
    pozycja_gracza = {x=1010, y=897, z=7},
    pozycja_potwora = {x=1012, y=899, z=7},
    nazwa_potwora = "Demon",
    czas = 30
}

function onUse(cid, item, frompos, item2, topos)
    if item.itemid == 1945 then
        if getPlayerPosition(cid) == config.pozycja_gracza
                doSummonCreature(config.nazwa_potwora, config.pozycja_potwora)                    
        else
            doPlayerSendTextMessage(cid, MessageClass, "Musisz stanac na wyznaczonym polu")
            end
        end
    end
end