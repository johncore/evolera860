function onUse(cid, item, fromPosition, itemEx, toPosition)
	if getCreatureCondition(cid, CONDITION_INFIGHT) then
		doPlayerSendCancel(cid, 'You can\'t teleport while in fight.')
	else
		doRemoveItem(item.uid)
		local old, new = getThingPos(cid), getPlayerMasterPos(cid)
		doTeleportThing(cid, new)
		doSendMagicEffect(old, CONST_ME_TELEPORT)
		doSendMagicEffect(new, CONST_ME_TELEPORT)
		doCreatureSay(cid, 'You have teleported to your home temple.', TALKTYPE_ORANGE_1, false, cid)
	end
	return true
end