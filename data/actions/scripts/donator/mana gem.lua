local storage = 10239 -- epmty storage
 
local shield = createConditionObject(CONDITION_MANASHIELD)
setConditionParam(shield, CONDITION_PARAM_TICKS, -1)
function onUse(cid, item, frompos, item2, topos)
	if getPlayerStorageValue(cid,storage) < 1 then 
		doAddCondition(cid,shield)
		doSendMagicEffect(getThingPos(cid),12)
		doPlayerSendTextMessage(cid,27,"Mana shield : On.")
		setPlayerStorageValue(cid,storage,1)
	else
		doRemoveCondition(cid,CONDITION_MANASHIELD)
		doPlayerSendTextMessage(cid,27,"Mana shield : Off.")
		setPlayerStorageValue(cid,storage,-1)
	end
	return true
end