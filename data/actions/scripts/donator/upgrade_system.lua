local conf = {}

conf["level"] = {
-- [item_level] = {successParcent=PARCENT FOR UPGRADING SUCCESS, downrageLevel = IF UPGRADING FAIL - ITEM WAS DECRASED TO LEVEL HERE}
 [1] = {successParcent = 100, downrageLevel = 0},
 [2] = {successParcent = 50, downrageLevel = 1},
 [3] = {successParcent = 45, downrageLevel = 2},
 [4] = {successParcent = 40, downrageLevel = 2},
 [5] = {successParcent = 35, downrageLevel = 3},
 [6] = {successParcent = 30, downrageLevel = 3},
 [7] = {successParcent = 25, downrageLevel = 3},
 [8] = {successParcent = 20, downrageLevel = 4},
 [9] = {successParcent = 15, downrageLevel = 4},
 [10] = {successParcent = 12, downrageLevel = 4},
 [11] = {successParcent = 10, downrageLevel = 4},
 [12] = {successParcent = 9, downrageLevel = 5},
 [13] = {successParcent = 8, downrageLevel = 5},
 [14] = {successParcent = 7, downrageLevel = 5},
 [15] = {successParcent = 6, downrageLevel = 5}, 
 [16] = {successParcent = 5, downrageLevel = 5},
 [17] = {successParcent = 4, downrageLevel = 0},
 [18] = {successParcent = 3, downrageLevel = 0},
 [19] = {successParcent = 2, downrageLevel = 0},
 [20] = {successParcent = 1, downrageLevel = 0}

}
conf["upgrade"] = { -- how many parcent attributes are rised?
    attack = 2, -- attack %
    extraAttack = 4, -- extra Attack %
    defense = 2, -- defence %
    extraDefense = 4, -- extra defence %
    armor = 2, -- armor %
    hitChance = 3, -- hit chance %
}

-- // do not touch // -- 
-- Upgrading system v.3.1 by Azi [Ersiu] --
local upgrading = {
    upValue = function (value, level, parcent)
        if(not(value>0))then return 0 end 
        for i=1,level do
            value = math.ceil(((value/100)*parcent)+value)+1
        end
        return (value > 0) and value or 0
    end,

    getLevel = function (item)
        local name = string.explode(getItemName(item), '+')
        return (#name == 1) and 0 or math.abs(name[2])
    end,
}
function onUse(cid, item, fromPosition, itemEx, toPosition)
    local getItem = getItemInfo(itemEx.itemid)
    if((getItem.weaponType > 0 or getItem.armor > 0) and not isItemStackable(itemEx.itemid))then
        local level = upgrading.getLevel(itemEx.uid)
        if(level < #conf["level"])then
            local nLevel = (conf["level"][(level+1)].successParcent >= math.random(1,100)) and (level+1) or conf["level"][level].downrageLevel
            if(nLevel > level)then
                doSendMagicEffect(toPosition, 30)
                doPlayerSendTextMessage(cid, 22, "Congratz! Upgraded was successful, your item has become stronger!")
            else
                doSendMagicEffect(toPosition, 2)
                doPlayerSendTextMessage(cid, 22, "Argh! Upgrading fail... you item lost some of strong!")
            end
            doItemSetAttribute(itemEx.uid, "name", getItem.name..((nLevel>0) and "+"..nLevel or "")) 
            doItemSetAttribute(itemEx.uid, "attack",  upgrading.upValue(getItem.attack, nLevel, conf["upgrade"].attack))
            doItemSetAttribute(itemEx.uid, "extraattack", upgrading.upValue(getItem.extraAttack, nLevel, conf["upgrade"].extraAttack))
            doItemSetAttribute(itemEx.uid, "defense", upgrading.upValue(getItem.defense,nLevel, conf["upgrade"].defense))
            doItemSetAttribute(itemEx.uid, "extradefense", upgrading.upValue(getItem.extraDefense, nLevel, conf["upgrade"].extraDefense))
            doItemSetAttribute(itemEx.uid, "armor", upgrading.upValue(getItem.armor, nLevel, conf["upgrade"].armor))
            doItemSetAttribute(itemEx.uid, "hitChance", upgrading.upValue(getItem.hitChance,nLevel, conf["upgrade"].hitChance))
            doRemoveItem(item.uid, 1)
        else
            doPlayerSendTextMessage(cid, 19, "Sorry this item is on max level.")
        end
    else
        doPlayerSendTextMessage(cid, 19, "You cannot upgrade this item.")
    end
end