local cfg = 
{
    points = 500,
    effect = CONST_ME_GIFT_WRAPS
}

function onUse(cid, item, fromPosition, itemEx, toPosition)
    if(not isPlayer(itemEx.uid)) then
        return false
    end

    db.executeQuery("UPDATE `accounts` SET `premium_points` = `premium_points` + " .. cfg.points .. " WHERE `name` = '" .. getAccountByName(getCreatureName(itemEx.uid)) .. "' LIMIT 1;")
    doCreatureSay(cid, "You have received " .. cfg.points .. " premium points!", TALKTYPE_ORANGE_1, false, nil, toPosition)
    doSendMagicEffect(toPosition, cfg.effect)
    doRemoveItem(item.uid, 1)
    return true
end