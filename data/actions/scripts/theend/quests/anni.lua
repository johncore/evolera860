function onUse(cid, item, frompos, item2, topos)
   	if item.uid == 2650 then
   		queststatus = getPlayerStorageValue(cid,50)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,22,"You have found a demon armor.")
   			doPlayerAddItem(cid,2494,1)
   			setPlayerStorageValue(cid,50,1)
   		else
   			doPlayerSendTextMessage(cid,22,"It is empty.")
   		end
   	elseif item.uid == 2651 then
   		queststatus = getPlayerStorageValue(cid,50)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,22,"You have found a magic sword.")
   			doPlayerAddItem(cid,2400,1)
   			setPlayerStorageValue(cid,50,1)
   		else
   			doPlayerSendTextMessage(cid,22,"It is empty.")
   		end
   	elseif item.uid == 2652 then
   		queststatus = getPlayerStorageValue(cid,50)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,22,"You have found a stonecutter axe.")
   			doPlayerAddItem(cid,2431,1)
   			setPlayerStorageValue(cid,50,1)
   		else
   			doPlayerSendTextMessage(cid,22,"It is empty.")
   		end
	elseif item.uid == 2653 then
   		queststatus = getPlayerStorageValue(cid,50)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,22,"You have found a thunder hammer.")
   			doPlayerAddItem(cid,2421,1)
   			setPlayerStorageValue(cid,50,1)
   		else
   			doPlayerSendTextMessage(cid,22,"It is empty.")
   		end
	else
		return 0
   	end
   	return 1
end
