function onUse(cid, item, frompos, item2, topos)
   	if item.uid == 2660 then
   		queststatus = getPlayerStorageValue(cid,51)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,22,"You have found a calamity.")
   			doPlayerAddItem(cid,8932,1)
   			setPlayerStorageValue(cid,51,1)
   		else
   			doPlayerSendTextMessage(cid,22,"It is empty.")
   		end
   	elseif item.uid == 2661 then
   		queststatus = getPlayerStorageValue(cid,51)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,22,"You have found the ice club.")
   			doPlayerAddItem(cid,7450,1)
   			setPlayerStorageValue(cid,51,1)
   		else
   			doPlayerSendTextMessage(cid,22,"It is empty.")
   		end
	elseif item.uid == 2662 then
   		queststatus = getPlayerStorageValue(cid,51)
   		if queststatus == -1 then
   			doPlayerSendTextMessage(cid,22,"You have found a impaler.")
   			doPlayerAddItem(cid,7435,1)
   			setPlayerStorageValue(cid,51,1)
   		else
   			doPlayerSendTextMessage(cid,22,"It is empty.")
   		end
	else
		return 0
   	end
   	return 1
end
