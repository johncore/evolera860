local config = {
	["earth stone"] = {
          	 [90] = {m = {'carniphila','earth elemental','bog raider'}, n = {6, 7}},
                	[80] = {m = {'bog raider','earth elemental'}, n = {6, 7}},
               	 [70] = {m = {'giant spider','bog raider'}, n = {6, 7}},
                	[60] = {m = {'hydra','lich','giant spider','bog raider'}, n = {6, 7}},
               	 [50] = {m = {'hydra','bog raider','serpent spawn'}, n = {6, 7}},
               	 [40] = {m = {'hydra','defiler','serpent spawn'}, n = {6, 7}},
                	[30] = {m = {'medusa','hydra','serpent spawn','defiler'}, n = {6, 7}},
               	 [20] = {m = {'medusa','lost soul','hydra','serpent spawn'}, n = {6, 7}},
             		 	  [10] = {m = {'juggernaut','son of verminor','medusa'}, n = {5, 6}},
               			 [1] = {m = {'medusa','juggernaut'}, n = {5,7}}

	},

	["wind stone"] = {
	 [90] = {m = {'gargoyle','stone golem','black knight'}, n = {6, 7}},
                	[80] = {m = {'black knight','behemoth'}, n = {5, 7}},
               	 [70] = {m = {'black knight','behemoth'}, n = {6, 7}},
                	[60] = {m = {'behemoth','destroyer'}, n = {6, 7}},
               	 [50] = {m = {'behemoth','destroyer','betrayed wraith'}, n = {6, 7}},
               	 [40] = {m = {'behemoth','destroyer','betrayed wraith'}, n = {6, 7}},
                	[30] = {m = {'dark torturer','betrayed wraith'}, n = {3, 5}},
               	 [20] = {m = {'dark torturer','grim reaper'}, n = {5, 6}},
             		 	  [10] = {m = {'blightwalker','undead dragon','grim reaper'}, n = {5, 6}},
               			 [1] = {m = {'undead dragon'}, n = {5,5}}

	},

	["icy stone"] = {
	 [90] = {m = {'ice golem','frost dragon'}, n = {6, 7}},
                	[80] = {m = {'frost dragon','frost dragon hatchling'}, n = {6, 7}},
               	 [70] = {m = {'frost dragon','crystal spider'}, n = {6, 7}},
                	[60] = {m = {'frost dragon','crystal spider'}, n = {7, 7}},
               	 [50] = {m = {'frost dragon','crystal spider'}, n = {7, 7}},
               	 [40] = {m = {'frost dragon'}, n = {7, 7}},
                	[30] = {m = {'frost dragon'}, n = {7, 8}},
               	 [20] = {m = {'frost dragon'}, n = {7, 8}},
             		 	  [10] = {m = {'yeti','frost dragon'}, n = {5, 7}},
               			 [1] = {m = {'yeti'}, n = {2,3}}

	},

	["fire stone"] = {
 	
	 [90] = {m = {'fire elemental','dragon','dragon lord','	dragon lord hatchling'}, n = {6, 7}},
                	[80] = {m = {'dragon lord hatchling','dragon lord'}, n = {6, 7}},
               	 [70] = {m = {'dragon lord','diabolic imp'}, n = {6, 7}},
                	[60] = {m = {'dragon lord','diabolic imp'}, n = {6, 7}},
               	 [50] = {m = {'dragon lord','demodras','diabolic imp'}, n = {6, 7}},
               	 [40] = {m = {'dragon lord','diabolic imp','hellfire fighter'}, n = {5, 7}},
                	[30] = {m = {'diabolic imp','hellfire fighter','demon'}, n = {6, 7}},
               	 [20] = {m = {'hellfire fighter','demon'}, n = {5, 6}},
             		 	  [10] = {m = {'demon','hellhound'}, n = {5, 5}},
               			 [1] = {m = {'demon'}, n = {5,5}}
	}

                }


 local function getPercent(number, all)
    return (number / all) * 100
end

local function choose(arg)
    return arg[math.random(#arg)]
end

local names = {'earth stone', 'icy stone', 'fire stone', 'wind stone'}

function onCombat(cid, target)

if isMonster(target) and isInArray(names, getCreatureName(target):lower()) then

    local t = getPercent(getCreatureHealth(target), getCreatureMaxHealth(target))
    if t > 90 then doCreatureSetStorage(target, 1000, 91) end
    for k, v in pairs(config[getCreatureName(target):lower()]) do
        if k >= t and getCreatureStorage(target, 1000) > k then
            local pos = getThingPos(target)
            for i = 1, math.random(v.n[1], v.n[2]) do
                local s = {x=pos.x,y=pos.y,z=pos.z}
                s.x = pos.x + math.random(-2,2)
                s.y = pos.y + math.random(-2,2)

                doCreateMonster(choose(v.m), pos, false, false, false)
            end
            return doCreatureSetStorage(target, 1000, k)
        end
    end
end
return true
end  