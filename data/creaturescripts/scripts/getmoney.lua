
local config = {
                levelReach = 300,
                item = 2160,
                count = 100
                }
 
function onAdvance(cid, skill, oldLevel, newLevel)
 
if(skill == SKILL__LEVEL and newLevel >= config.levelReach and getPlayerStorageValue(cid, 60000) == -1) then
        setPlayerStorageValue(cid, 60000), 1)
        doPlayerAddItem(cid, config.item, config.count)
end
 
return TRUE
end