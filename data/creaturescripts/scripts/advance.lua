	function onAdvance(cid, skill, oldlevel, newlevel)
                    local pPos =  getCreaturePosition(cid)
                    if skill == SKILL__MAGLEVEL then
                            doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "Gratulacje! Awansowales w Magic Levelu")
                            doSendMagicEffect(pPos, 40)
                    elseif skill == SKILL_SHIELD then
                            doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "Gratulacje! Awansowales w Obronie")
                            doSendMagicEffect(pPos, 3)
                    elseif skill == SKILL_AXE then
                            doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "Gratulacje! Awansowales w walce Toporami")
                            doSendMagicEffect(pPos, 42)
                    elseif skill == SKILL_SWORD then
                            doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "Gratulacje! Awansowales w walce Mieczami")
                            doSendMagicEffect(pPos, 50)
                    elseif skill == SKILL_CLUB then
                            doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "Gratulacje! Awansowales w walce Maczugami")
                            doSendMagicEffect(pPos, 30)   
                    elseif skill == SKILL_DISTANCE then
                            doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "Gratulacje! Awansowales w walce na Dystans")
                            doSendMagicEffect(pPos, 29)
                    elseif skill == SKILL__LEVEL then
                            doPlayerSendTextMessage(cid, MESSAGE_EVENT_ADVANCE, "Gratulacje! Awansowales w Levelu!")
                            doSendMagicEffect(pPos, 28)     
                    end
                            doCreatureAddHealth(cid, getCreatureMaxHealth(cid)-getCreatureHealth(cid))
                            doCreatureAddMana(cid, getCreatureMaxMana(cid)-getCreatureMana(cid))
                    return true
    end

