function onKill(cid, target)
-- Config --
local storage = 666 -- Storage value of the script.
local script =
{
    name = getCreatureName(target), -- Do not edit.
    how_check = getPlayerStorageValue(cid,999), -- Do not edit.
    killed = getPlayerStorageValue(cid,storage), -- Do not edit.
    skull_check = getCreatureSkullType(target) -- Do not edit.
} 
local how = 3 -- How many monsters need to get reput point.
local rep_for_monster = 1 -- How many reputation give for kill monster.
local rep_for_player = 5 -- How many reputation give for kill player without skull.
local rep_for_whiteskull = 10 -- How many reputation give for kill player with white skull.
local rep_for_redskull = 15 -- How many reputation give for kill player with red skull.
local rep_for_blackskull = 20 -- How many reputation give for kill player with black skull.
local points = script.killed + 1
local msg = "You have now ".. points .." points."
local msg_monster = "Congratulations, you have slained ".. how .." monsters and you gained ".. rep_for_monster.." reputation point." -- Standard message for killing monster you can edit this.
local msg_player = "Congratulations, you have killed a ".. script.name .." and you gained ".. rep_for_player .." reputation points." -- Standard message for killing player without skull you can edit this.
local msg_white = "Congratulations, you have killed a ".. script.name .." and you gained ".. rep_for_whiteskull .." reputation points." -- Standard message for killing player with white skull you can edit this.
local msg_red = "Congratulations, you have killed a ".. script.name .." and you gained ".. rep_for_redskull .." reputation points." -- Standard message for killing player with red skull you can edit this.
local msg_black = "Congratulations, you have killed a ".. script.name .." and you gained ".. rep_for_blackskull .." reputation points." -- Standard message for killing player with black skull you can edit this.
 
-- Script --
if(getPlayerStorageValue(cid,999) >= how -1) then
		setPlayerStorageValue(cid, storage,script.killed + rep_for_monster)
		doPlayerSendTextMessage(cid,22,msg_monster)
		doPlayerSendTextMessage(cid,23,msg)
		setPlayerStorageValue(cid, 999,0)
		return TRUE
elseif(isMonster(target) == TRUE) then
		setPlayerStorageValue(cid, 999,script.how_check + 1)
		return TRUE
elseif(isPlayer(target) == TRUE) then
		setPlayerStorageValue(cid, storage,script.killed + rep_for_player)
		doPlayerSendTextMessage(cid,22,msg_player)
		doPlayerSendTextMessage(cid,23,msg)
		return TRUE
elseif(isPlayer(target) == TRUE) and skull_check == 3 then
		setPlayerStorageValue(cid, storage,script.killed + rep_for_whiteskull)
		doPlayerSendTextMessage(cid,22,msg_white)
		doPlayerSendTextMessage(cid,23,msg)
		return TRUE
elseif(isPlayer(target) == TRUE) and skull_check == 4 then
		setPlayerStorageValue(cid, storage,script.killed + rep_for_redskull)
		doPlayerSendTextMessage(cid,22,msg_red)
		doPlayerSendTextMessage(cid,23,msg)
		return TRUE
elseif(isPlayer(target) == TRUE) and skull_check == 5 then
		setPlayerStorageValue(cid, storage,script.killed + rep_for_blackskull)
		doPlayerSendTextMessage(cid,22,msg_black)
		doPlayerSendTextMessage(cid,23,msg)
		return TRUE
	end
end