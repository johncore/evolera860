local config = {
                storage = 1233,
                [100] = {rewards = {'points',25,'cash',5000}},
                [150] = {rewards = {'points',50,'cash',25000}},
                [200] = {rewards = {'points',100,'cash',35000}},
                [250] = {rewards = {'points',200,'cash',50000}},
                [300] = {rewards = {'points',250,'cash',75000}},
                }

local function doPlayerAddPremiumPoints(cid, points)
return db.executeQuery("UPDATE `accounts` SET `premium_points` = `premium_points`+"..points.." WHERE `id` = "..getPlayerAccountId(cid)..";")
end

function onAdvance(cid, type, oldlevel, newlevel)
local l = config[newlevel]
if l and getPlayerStorageValue(cid, config.storage) < newlevel then
    for i = 1, #v.rewards/2 do
        if v.rewards[i*2-1] == 'points' then
            doPlayerAddPremiumPoints(cid, v.rewards[i*2])
        elseif v.rewards[i*2-1] == 'cash' then
            doPlayerAddMoney(cid, v.rewards[i*2])
        end
    end
    return doPlayerSetStorageValue(cid, config.storage, newlevel)
end
return true
end 