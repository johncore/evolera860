function onLook(cid, thing, position, lookDistance)
local quests = {9693, 115, 50, 8889, 26, 114, 91, 101, 9999, 90, 91, 73, 19, 80, 60, 30, 10503, 75, 70, 81, 96, 2, 71, 10, 20, 131, 40, 155, 157, 178, 179, 89, 193, 215, 216, 217, 92, 133, 93, 77, 21, 145, 4, 5010, 1, 93, 124, 22221, 221, 5907, 99, 143, 3, 41, 31, 7, 7735, 92, 98, 18, 139, 65, 64, 25, 9778, 12610, 12606}
local completed = {}
	if isPlayer(thing.uid) then
		for i = 1, #quests do
			if getPlayerStorageValue(thing.uid, quests[i]) > 0 then
				table.insert(completed, 1)
			end
		end
		doPlayerSetSpecialDescription(thing.uid, (getPlayerSex(thing.uid) == 0 and ".\nShe" or ".\nHe") .. " has completed ".. #completed .. "/" .. #quests .. " quests")
		doPlayerSendTextMessage(cid, 27, getPlayerName(thing.uid) .. " has completed " .. #completed .. "/" .. #quests .. " quests.")
	end
return true
end