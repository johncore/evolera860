local names = {'earth stone', 'icy stone', 'fire stone', 'wind stone'}
local rewards = {{2160, 50}, {5080, 1}, {9969, 1}, {5785, 1}, {2349, 1}}


function onDeath(cid, corpse, deathList)
	if (isInArray(names, getCreatureName(cid):lower())) then
		--local mostdmg = deathList[2] ~= nil and deathList[2] or deathList[1]
		local mostdmg = isPlayer(deathList[1]) and deathList[1] or (deathList[2] ~= nil and deathList[2])
		if isPlayer(mostdmg) then
			local rand = math.random(1, #rewards)
		
			doPlayerAddItem(mostdmg, rewards[rand][1], rewards[rand][2])
			doPlayerSendTextMessage(mostdmg, 22, "You have killed " .. getCreatureName(cid) .. ". You won " .. rewards[rand][2] .. " " .. getItemNameById(rewards[rand][1]) .. ".")
			db.executeQuery("INSERT INTO metin_wins (name, reward, reward_id, stone, date) VALUES ('" .. getCreatureName(mostdmg) .. "', '" .. getItemNameById(rewards[rand][1]) .. "', '" .. rewards[rand][1] .. "', '" .. getCreatureName(cid) .. "', " .. os.time() .. ")") 
		
		else
			doBroadcastMessage("No one has destroyed the stone")
		end
	end
	return TRUE
end