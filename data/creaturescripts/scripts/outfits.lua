local hunter = createConditionObject(CONDITION_ATTRIBUTES)
setConditionParam(hunter, CONDITION_PARAM_TICKS, -1)
setConditionParam(hunter, CONDITION_PARAM_SKILL_DISTANCE, 5)
local knight = createConditionObject(CONDITION_ATTRIBUTES)
setConditionParam(knight, CONDITION_PARAM_TICKS, -1)
setConditionParam(knight, CONDITION_PARAM_SKILL_SWORD, 5)
local mage = createConditionObject(CONDITION_ATTRIBUTES)
setConditionParam(mage, CONDITION_PARAM_TICKS, -1)
setConditionParam(mage, CONDITION_PARAM_STAT_MAGICLEVEL, 3)
local yalahar = createConditionObject(CONDITION_ATTRIBUTES)
setConditionParam(yalahar, CONDITION_PARAM_TICKS, -1)
setConditionParam(yalahar, CONDITION_PARAM_STAT_MAGICLEVEL, 2)
local barbarian = createConditionObject(CONDITION_ATTRIBUTES)
setConditionParam(barbarian, CONDITION_PARAM_TICKS, -1)
setConditionParam(barbarian, CONDITION_PARAM_SKILL_AXE, 5)
local norse = createConditionObject(CONDITION_ATTRIBUTES)
setConditionParam(norse, CONDITION_PARAM_TICKS, -1)
setConditionParam(norse, CONDITION_PARAM_SKILL_SHIELD, 5)
local nightmare = createConditionObject(CONDITION_ATTRIBUTES)
setConditionParam(norse, CONDITION_PARAM_TICKS, -1)
setConditionParam(norse, CONDITION_PARAM_SKILL_SHIELD, 5)
local Bunter = createConditionObject(CONDITION_ATTRIBUTES)
setConditionParam(Bunter, CONDITION_PARAM_TICKS, -1)
setConditionParam(Bunter, CONDITION_PARAM_STAT_MAGICLEVEL, 3)
function onOutfit(cid, old, current)
        if(current.lookType == old.lookType) then
                return TRUE
        end
        --Citizen
        if(current.lookType == 128 and current.lookAddons == 3 or current.lookType == 136 and current.lookAddons == 3) then --Bonus on
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 150)
                doCreatureAddHealth(cid, 100)
                doChangeSpeed(cid, 10)
        elseif(old.lookType == 128 and old.lookAddons == 3 or old.lookType == 136 and old.lookAddons == 3) then --Bonus off
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 150)
                doCreatureAddHealth(cid, -100)
                doChangeSpeed(cid, -10) 
        end
        --Hunter
        if(current.lookType == 129 and current.lookAddons == 3 or current.lookType == 137 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, hunter)
        elseif(old.lookType == 129 and old.lookAddons == 3 or old.lookType == 137 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
        end
        --Mage
        if(current.lookType == 130 and current.lookAddons == 3 or current.lookType == 138 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, mage)
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) + 400)
                doCreatureAddMana(cid, 400)
        elseif(old.lookType == 130 and old.lookAddons == 3 or old.lookType == 138 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) - 400)
                doCreatureAddMana(cid, -400)
        end
        --Knight
        if(current.lookType == 131 and current.lookAddons == 3 or current.lookType == 139 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, knight)
        elseif(old.lookType == 131 and old.lookAddons == 3 or old.lookType == 139 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
        end
        --Summoner
        if(current.lookType == 133 and current.lookAddons == 3 or current.lookType == 141 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, mage)
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) + 300)
                doCreatureAddMana(cid, 300)
        elseif(old.lookType == 133 and old.lookAddons == 3 or old.lookType == 141 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) - 300)
                doCreatureAddMana(cid, -300)
        end
        --Warrior
        if(current.lookType == 134 and current.lookAddons == 3 or current.lookType == 142 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, knight)
        elseif(old.lookType == 134 and old.lookAddons == 3 or old.lookType == 142 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
        end
        --Barbarian
        if(current.lookType == 143 and current.lookAddons == 3 or current.lookType == 147 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, barbarian)
        elseif(old.lookType == 143 and old.lookAddons == 3 or old.lookType == 147 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
        end
        --Druid
        if(current.lookType == 144 and current.lookAddons == 3 or current.lookType == 148 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, mage)
        elseif(old.lookType == 144 and old.lookAddons == 3 or old.lookType == 148 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
        end
        --Wizard
        if(current.lookType == 145 and current.lookAddons == 3 or current.lookType == 149 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, mage)
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) + 300)
                doCreatureAddMana(cid, 300)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 50)
                doCreatureAddHealth(cid, 50)
        elseif(old.lookType == 145 and old.lookAddons == 3 or old.lookType == 149 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) - 300)
                doCreatureAddMana(cid, -300)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 50)
                doCreatureAddHealth(cid, -50)
        end
        --Oriental
        if(current.lookType == 146 and current.lookAddons == 3 or current.lookType == 150 and current.lookAddons == 3) then --Bonus on
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) + 100)
                doCreatureAddMana(cid, 100)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 150)
                doCreatureAddHealth(cid, 150)
                doChangeSpeed(cid, 80)
        elseif(old.lookType == 146 and old.lookAddons == 3 or old.lookType == 150 and old.lookAddons == 3) then --Bonus off
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) - 100)
                doCreatureAddMana(cid, -100)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 150)
                doCreatureAddHealth(cid, -150)
                doChangeSpeed(cid, -80)
        end
        --Assassin
        if(current.lookType == 152 and current.lookAddons == 3 or current.lookType == 156 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, hunter)
                doChangeSpeed(cid, 200)
        elseif(old.lookType == 152 and old.lookAddons == 3 or old.lookType == 156 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
                doChangeSpeed(cid, -200)
        end
        --Shaman
        if(current.lookType == 154 and current.lookAddons == 3 or current.lookType == 158 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, mage)
        elseif(old.lookType == 154 and old.lookAddons == 3 or old.lookType == 158 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
        end
        --Norse
        if(current.lookType == 251 and current.lookAddons == 3 or current.lookType == 252 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, norse)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 200)
                doCreatureAddHealth(cid, 200)
        elseif(old.lookType == 251 and old.lookAddons == 3 or old.lookType == 252 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 200)
                doCreatureAddHealth(cid, -200)
        end
        --Nightmare
        if(current.lookType == 268 and current.lookAddons == 3 or current.lookType == 269 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, nightmare)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 200)
                doCreatureAddHealth(cid, 200)
        elseif(old.lookType == 268 and old.lookAddons == 3 or old.lookType == 269 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 200)
                doCreatureAddHealth(cid, -200)
        end
        --Jester
        if(current.lookType == 270 and current.lookAddons == 3 or current.lookType == 273 and current.lookAddons == 3) then --Bonus on
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) + 100)
                doCreatureAddMana(cid, 100)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 100)
                doCreatureAddHealth(cid, 100)
                doChangeSpeed(cid, 10)
        elseif(old.lookType == 270 and old.lookAddons == 3 or old.lookType == 273 and old.lookAddons == 3) then --Bonus off
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) - 100)
                doCreatureAddMana(cid, -100)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 100)
                doCreatureAddHealth(cid, -100)
                doChangeSpeed(cid, -10)
        end
        --Brotherhood
        if(current.lookType == 278 and current.lookAddons == 3 or current.lookType == 279 and current.lookAddons == 3) then --Bonus on
                doAddCondition(cid, mage)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 200)
                doCreatureAddHealth(cid, 200)
        elseif(old.lookType == 278 and old.lookAddons == 3 or old.lookType == 279 and old.lookAddons == 3) then --Bonus off
                doRemoveCondition(cid, CONDITION_ATTRIBUTES)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 200)
                doCreatureAddHealth(cid, -200)
        end
        --Demonhunter
        if(current.lookType == 288 and current.lookAddons == 3 or current.lookType == 289 and current.lookAddons == 3) then --Bonus on
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) + 300)
                doCreatureAddMana(cid, 300)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 150)
                doCreatureAddHealth(cid, 150)
                doChangeSpeed(cid, 20)
        elseif(old.lookType == 288 and old.lookAddons == 3 or old.lookType == 289 and old.lookAddons == 3) then --Bonus off
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) - 300)
                doCreatureAddMana(cid, -300)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 150)
                doCreatureAddHealth(cid, -150)
                doChangeSpeed(cid, -20)
        end
        --Yalaharian
        if(current.lookType == 324 and current.lookAddons == 3 or current.lookType == 325 and current.lookAddons == 3) then --Bonus on
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) + 200)
                doCreatureAddMana(cid, 200)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 200)
                doCreatureAddHealth(cid, 200)
                doAddCondition(cid, yalahar)
        elseif(old.lookType == 324 and old.lookAddons == 3 or old.lookType == 325 and old.lookAddons == 3) then --Bonus off
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) - 200)
                doCreatureAddMana(cid, -200)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 200)
                doCreatureAddHealth(cid, -200)
                doRemoveCondition(cid, yalahar)
        end
        --Bunter
        if(current.lookType == 367 and current.lookAddons == 3 or current.lookType == 367 and current.lookAddons == 3) then --Bonus on
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) + 500)
                doCreatureAddMana(cid, 500)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 100)
                doCreatureAddHealth(cid, 100)
                doAddCondition(cid, Bunter)
        elseif(old.lookType == 367 and old.lookAddons == 3 or old.lookType == 367 and old.lookAddons == 3) then --Bonus off
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) - 500)
                doCreatureAddMana(cid, -500)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 100)
                doCreatureAddHealth(cid, -100)
                doRemoveCondition(cid, Bunter)
        end
        --war male
        if(current.lookType == 335 and current.lookAddons == 3 or current.lookType == 335 and current.lookAddons == 3) then --Bonus on
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) + 100)
                doCreatureAddMana(cid, 100)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 500)
                doCreatureAddHealth(cid, 500)
                doAddCondition(cid, Warmaster)
        elseif(old.lookType == 335 and old.lookAddons == 3 or old.lookType == 335 and old.lookAddons == 3) then --Bonus off
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) - 100)
                doCreatureAddMana(cid, -100)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 500)
                doCreatureAddHealth(cid, -500)
                doRemoveCondition(cid, Warmaster)
        end
        --war female
        if(current.lookType == 336 and current.lookAddons == 3 or current.lookType == 336 and current.lookAddons == 3) then --Bonus on
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) + 100)
                doCreatureAddMana(cid, 100)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) + 500)
                doCreatureAddHealth(cid, 500)
                doAddCondition(cid, Warmaster)
        elseif(old.lookType == 336 and old.lookAddons == 3 or old.lookType == 336 and old.lookAddons == 3) then --Bonus off
                setCreatureMaxMana(cid, getCreatureMaxMana(cid) - 100)
                doCreatureAddMana(cid, -100)
                setCreatureMaxHealth(cid, getCreatureMaxHealth(cid) - 500)
                doCreatureAddHealth(cid, -500)
                doRemoveCondition(cid, Warmaster)
        end
        return TRUE
end
