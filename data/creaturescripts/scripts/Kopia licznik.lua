function onKill(cid, target, lastHit)
 if isMonster(target) then
  local storage = 20000
  for i = 1, getCreatureName(target):len() do
   storage = storage + getCreatureName(target):byte(i,i)
  end
  if getPlayerStorageValue(cid,storage) < 0 then
   setPlayerStorageValue(cid,storage,0)
  end
  setPlayerStorageValue(cid,storage,getPlayerStorageValue(cid,storage) + 1)
  doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_BLUE, "You have already killed " .. getCreatureName(target):lower() .. " " .. getPlayerStorageValue(cid,storage) .. " times.")
 end
return true
end