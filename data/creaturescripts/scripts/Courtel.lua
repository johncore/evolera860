local monsters = {
                [2956] = {monster = "weak vampire"},
                [2916] = {monster = "orshabaal"}
                }
local tiles = {2957,2958,4299,4300}

local fields = {
                [1] = {item_id = {1497,1499}, effect = CONST_ME_TELEPORT},
                [2] = {item_id = {1492,1493,1494}, effect = CONST_ME_MAGIC_RED},
                [3] = {item_id = {1495}, effect = CONST_ME_MAGIC_BLUE},
                [4] = {item_id = {1496}, effect = CONST_ME_MAGIC_GREEN},
                [5] = {item_id = {967}, effect = CONST_ME_MAGIC_RED},
                [6] = {item_id = {11779}, effect = CONST_ME_ASSASSIN}
                }

local function effect(p)
local fieldFound = getThingFromPos(p.pos)
    if isCreature(p.cid) then
        doSendMagicEffect(p.pos, CONST_ME_MORTAREA)
        if p.n < 4 then
            p.n=p.n+1
            addEvent(effect, 250, p)
        else
            if monsters[fieldFound.itemid] then 
                doCreateMonster(monsters[fieldFound.itemid].monster, p.pos, false)
                doRemoveItem(fieldFound.uid)
            elseif isInArray(tiles, fieldFound.itemid) then
                doCreateItem(fieldFound.itemid-1, 1, p.pos)
                doRemoveItem(fieldFound.uid)
            end
        end
    end
end

function onThink(cid)
    if isMonster(cid) then
        local pos = getCreaturePosition(cid)
        if math.random(1, 100) <= 7 then
            if getCreatureHealth(cid) < getCreatureMaxHealth(cid) then
                for nx = (pos.x - 3), (pos.x + 3) do
                    for ny = (pos.y - 3), (pos.y + 3) do
                        local creatureFound = getThingFromPos({x=nx,y=ny,z=pos.z,stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE})
                        if creatureFound.uid ~= cid and isMonster(creatureFound.uid) and getCreatureName(creatureFound.uid) == "orshabaal" and getCreatureMaster(creatureFound.uid) == cid and isSightClear(getCreaturePosition(creatureFound.uid), pos, true) then
                            local hp = math.ceil(getCreatureHealth(creatureFound.uid)+math.random(1,9))
                            doCreatureAddHealth(cid, hp)
                            doSendAnimatedText(pos, hp, COLOR_BLACK)
                            doSendDistanceShoot(getThingPos(creatureFound.uid), pos, CONST_ANI_DEATH)
                            doSendMagicEffect(pos, CONST_ME_MORTAREA)
                        end
                    end
                end
            end
        end
        if math.random(1, 100) <= 15 then
            for nx = (pos.x - 2), (pos.x + 2) do
                for ny = (pos.y - 2), (pos.y + 2) do
                    for i = 1, 254 do
                        stackpos = i
                        local fieldFound = getThingFromPos({x = nx, y = ny, z = pos.z, stackpos = stackpos})
                        if fieldFound.uid ~= 0 and isInArray({2956,2957,2958,4298,4299,4300}, fieldFound.itemid) and isSightClear(pos, getThingPos(fieldFound.uid), true) then
                            local poss = getThingPos(fieldFound.uid)
                            addEvent(effect, 250, {pos=poss, cid=cid, n=1, fieldFound=fieldFound})
                        end
                    end
                end
            end
        end
        if math.random(1, 100) <= 10 then 
            for nx = (pos.x - 1), (pos.x + 1) do 
                for ny = (pos.y - 1), (pos.y + 1) do 
                    for i = 1, 254 do
                        stackpos = i
                        local fieldFound = getThingFromPos({x = nx, y = ny, z = pos.z, stackpos = stackpos}) 
                        for k = 1, #fields do
                            if fieldFound.uid ~= 0 and isInArray(fields[k].item_id, fieldFound.itemid) then
                                doSendMagicEffect(getThingPos(fieldFound.uid), fields[k].effect)
                                doRemoveItem(fieldFound.uid)
                            end
                        end
                    end
                end
            end
        end
    end
    return true
end  