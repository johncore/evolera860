function onDeath(cid, corpse, killer)
local storage = 1
local playerPos = getPlayerPosition(cid)
setPlayerStorageValue(cid, storage, playerPos.x)
setPlayerStorageValue(cid, storage+1, playerPos.y)
setPlayerStorageValue(cid, storage+2, playerPos.z)
setPlayerStorageValue(cid, storage+3, os.time() + 600)
return TRUE
end
