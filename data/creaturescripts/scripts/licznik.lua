function onKill(cid, target, lastHit)
 if isMonster(target) then
  local storage = 20000
  for i = 1, getCreatureName(target):len() do
   storage = storage + getCreatureName(target):byte(i,i)
  end
  if getPlayerStorageValue(cid,storage) < 0 then
   setPlayerStorageValue(cid,storage,0)
  end
  setPlayerStorageValue(cid,storage,getPlayerStorageValue(cid,storage) + 1)
  doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "Now you have killed " .. getPlayerStorageValue(cid,storage) .. " " .. getCreatureName(target):lower() .. "'s.")
 end
return true
end