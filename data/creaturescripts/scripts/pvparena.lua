config = {
    pvpParcent = 3, -- how many exp(player killed exp parcent) killers gain for kill.
    -- arena must be square!!
    arenaFrom = {980, 988, 7}, -- pvp arena from x,y,z position
    arenaTo = {987, 995, 7}, -- pvp arena to x,y,z position 

}


function gainExperience(cid,lastHitKiller, mostDamageKiller)
    if (isPlayer(lastHitKiller) and isPlayer(mostDamageKiller)) and (lastHitKiller ~= mostDamageKiller) then
        doPlayerAddExp(lastHitKiller, (getPlayerExperience(cid)/200)*config.pvpParcent)
        doSendAnimatedText(getPlayerPosition(lastHitKiller), (getPlayerExperience(cid)/200)*config.pvpParcent, TEXTCOLOR_WHITE)
        doPlayerAddExp(mostDamageKiller, (getPlayerExperience(cid)/200)*config.pvpParcent)
        doSendAnimatedText(getPlayerPosition(mostDamageKiller), (getPlayerExperience(cid)/200)*config.pvpParcent, TEXTCOLOR_WHITE)
    elseif (isPlayer(lastHitKiller) and not(isPlayer(mostDamageKiller))) then
        doPlayerAddExp(lastHitKiller, (getPlayerExperience(cid)/100)*config.pvpParcent)
        doSendAnimatedText(getPlayerPosition(lastHitKiller), (getPlayerExperience(cid)/100)*config.pvpParcent, TEXTCOLOR_WHITE)
    elseif (not(isPlayer(lastHitKiller)) and isPlayer(mostDamageKiller)) then    
        doPlayerAddExp(mostDamageKiller, (getPlayerExperience(cid)/100)*config.pvpParcent)
        doSendAnimatedText(getPlayerPosition(mostDamageKiller), (getPlayerExperience(cid)/100)*config.pvpParcent, TEXTCOLOR_WHITE)
    end
end

function onPrepareDeath(cid, lastHitKiller, mostDamageKiller)

    local playerPos = getPlayerPosition(cid)
    local killerPos = getPlayerPosition(lastHitKiller)
    if isPlayer(cid) then
        if (playerPos.x >= config.arenaFrom[1] and playerPos.x <= config.arenaTo[1]) and (playerPos.y >= config.arenaFrom[2] and playerPos.y <= config.arenaTo[2]) and (playerPos.z >= config.arenaFrom[3] and playerPos.z <= config.arenaTo[3]) then
            if (killerPos.x >= config.arenaFrom[1] and killerPos.x <= config.arenaTo[1]) and (killerPos.y >= config.arenaFrom[2] and killerPos.y <= config.arenaTo[2]) and (killerPos.z >= config.arenaFrom[3] and killerPos.z <= config.arenaTo[3]) then
                gainExperience(cid,lastHitKiller, mostDamageKiller)
            end
        end
    end
        return TRUE
end