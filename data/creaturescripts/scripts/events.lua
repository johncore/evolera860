            --Script By Unass--
local skills = 
{
    [1] = {points = 50, storage = 30701, value = 2, text = "level", level = 100}
    [2] = {points = 75, storage = 30702, value = 2, text = "level", level = 150}
    [3] = {points = 100, storage = 30703, value = 2, text = "level", level = 200}
    [4] = {points = 150, storage = 30704, value = 2, text = "level", level = 250}			  
	[5] = {points = 200, storage = 30705, value = 2, text = "level", level = 300}
}

function onAdvance(cid, skill, oldLevel, newLevel)
    local x = skills[skill]
    if getGlobalStorageValue(x.storage) < x.value and getPlayerStorageValue(cid, x.storage) ~= x.value and getPlayerSkillLevel(cid, skill) >= x.level and skills[skill] then
    doAddPoints(cid, x.points)
    setGlobalStorageValue(x.storage, x.value)
    setPlayerStorageValue(cid, x.storage, x.value)
    doPlayerSendTextMessage(cid, MESSAGE_STATUS_CONSOLE_ORANGE, "Congratulations! You are reached ".. x.level .." ".. x.text .." and you have gained ".. x.points .." Premium Points!")
    end
    return true
end