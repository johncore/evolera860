-- Tasks System by Matee
-- Skrypt pochodzi z tibia.net.pl

function onKill(cid, target)
local MATTSTER = {
	["Demon"] = 	{npc = 5050, npc_stan = 2, limit = 5000, quest_id = 5001},
	["Rotworm"] = 	{npc = 5050, npc_stan = 3, limit = 450, quest_id = 5002},
	["Dragon"] = 	{npc = 5050, npc_stan = 4, limit = 2500, quest_id = 5003}
	["Wyrm"] = 	{npc = 5050, npc_stan = 5, limit = 3000, quest_id = 5004}
	["Behemoth"] = 	{npc = 5050, npc_stan = 6, limit = 3000, quest_id = 5005}
	["Diabolic Imp"] = 	{npc = 5050, npc_stan = 7, limit = 10000, quest_id = 5006}
	["Hellfire Fighter"] = 	{npc = 5050, npc_stan = 8, limit = 5000, quest_id = 5007}
	["Dragon Lord"] = 	{npc = 5050, npc_stan = 9, limit = 4000, quest_id = 5008},
	["Hydra"] = 	{npc = 5050, npc_stan = 10, limit = 8000, quest_id = 5009},
	["Hellhound"] = 	{npc = 5050, npc_stan = 11, limit = 5000, quest_id = 5010},
	["Nightmare"] = 	{npc = 5050, npc_stan = 12, limit = 8000, quest_id = 5011},
}
	local task = MATTSTER[getCreatureName(target)]
	if(not task) then
		return true
	end
	if getPlayerStorageValue(cid, task.npc) == task.npc_stan then
		if getPlayerStorageValue(cid, task.quest_id) <= task.limit + 1 then
			local monster = getPlayerStorageValue(cid, task.quest_id) + 1
			local monster_number = getPlayerStorageValue(cid, task.quest_id)
			local monster_mate = getCreatureName(target)
			setPlayerStorageValue(cid, task.quest_id, monster)
			doPlayerSay(cid, "You kill "..monster_number.." "..monster_mate..".", TALKTYPE_MONSTER)
		end
	end
	return true
end