function onLogin(cid)

 local serverName = getConfigInfo('Real Tibia')
 local storage = getPlayerStorageValue(cid, 1000)
 
 if storage == -1 then
 doPlayerSendTextMessage(cid, 22, "Hello "..getPlayerName(cid).." this is your first visit to "..serverName..". Enjoy your time on "..serverName..".")
 setPlayerStorageValue(cid, 1000, 1)
 else
 doPlayerSendTextMessage(cid, 22, "Welcome back "..getPlayerName(cid).."!")
 return TRUE
end  