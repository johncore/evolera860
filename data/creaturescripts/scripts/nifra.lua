local bagno_zwykle = {4691,4692,4693,4694,4695,4696,4697,4698,4699,4700,4701,4702,4703,4704,4705,4706,4707,4708,4709,4710,4711,4712,4749,4750,4751,4752,4753,4754,4755}
local bagno_walkable = 6353
local pos = getCreaturePosition

    local function efekty(p) 
    local cid = p.cid
    local loc = p.loc
        if isCreature(cid) then
            doSendMagicEffect(loc, CONST_ME_HITBYPOISON)
            doSendMagicEffect(loc, CONST_ME_GREEN_RINGS)
                if p.s < 6 then
                    addEvent(efekty, 250, p)
                    p.s = p.s + 1
                else
                    addEvent(kill, 1000, {cid = cid, loc = loc})
                end
        end
    end    

    local function kill(params) 
    local cid = params.cid
    local loc = params.loc
        if isCreature(cid) then 
            doSendMagicEffect(loc, CONST_ME_SMALLPLANTS) 
            local creatureFound = getThingFromPos(loc) 
            if isCreature(creatureFound.uid) and creatureFound.uid ~= cid then
                doTargetCombatHealth(0, creatureFound.uid, COMBAT_EARTHDAMAGE, -getCreatureHealth(creatureFound.uid)*0.25, -getCreatureHealth(creatureFound.uid)*0.75, CONST_ME_SMALLPLANTS)
            end
        end
    end

function onThink(cid)
local tbl_x, tbl_y, tbl_z = {}, {}, {}
if isMonster(cid) then
    if math.random(1, 100) <= 60 then
        for nx = (pos(cid).x - 5), (pos(cid).x + 5) do
            for ny = (pos(cid).y - 5), (pos(cid).y + 5) do
                local itemFound = getTileThingByPos({x=nx, y=ny, z=pos(cid).z})
                if itemFound.uid > 0 and isInArray(bagno_zwykle, itemFound.itemid) then
                    local item_pos = getThingPos(itemFound.uid)
                    table.insert(tbl_x, item_pos.x)
                    table.insert(tbl_y, item_pos.y)
                    table.insert(tbl_z, item_pos.z)
                end
            end
        end
        if unpack(tbl_x) ~= nil and unpack(tbl_y) ~= nil and unpack(tbl_z) ~= nil then
            local x_pos, y_pos, z_pos = tbl_x[math.random(1, #tbl_x)], tbl_y[math.random(1, #tbl_y)], tbl_z[math.random(1, #tbl_z)]
            local poss = {x=x_pos, y=y_pos, z=z_pos, stackpos=0}
            local underItemFound_1, bagno = getThingFromPos({x=poss.x, y=poss.y, z=poss.z, stackpos=1}), getTileThingByPos(poss).uid
            local itemFound = getThingFromPos(poss)
                if itemFound.uid > 0 and isInArray(bagno_zwykle, itemFound.itemid) then
                    local id = getThingFromPos(poss).itemid
                    if math.random(1,10) <= 7 then
                        for sx = (pos(cid).x - 5), (pos(cid).x + 5) do
                        for sy = (pos(cid).y - 5), (pos(cid).y + 5) do
                        local creatureFound = getThingFromPos({x = sx, y = sy, z = pos(cid).z, stackpos = STACKPOS_TOP_MOVEABLE_ITEM_OR_CREATURE})
                        if creatureFound.uid ~= cid and isCreature(creatureFound.uid) and creatureFound.uid == getCreatureTarget(cid) and isSightClear(pos(cid), pos(creatureFound.uid), true) and isSightClear(poss, pos(creatureFound.uid), true) then
                            local posit = getThingPos(creatureFound.uid)
                            if underItemFound_1.uid == 0 then
                                doTransformItem(bagno, bagno_walkable)
                                doCreateMonster("Swampy Bilith", poss, false)
                            else
                                return true
                            end
                                doSendMagicEffect(poss, CONST_ME_GREEN_RINGS)
                                doTransformItem(bagno, id)
                                doSendDistanceShoot(poss, posit, CONST_ANI_EARTH)
                                doSendMagicEffect(posit, CONST_ME_HITBYPOISON)
                                doSendMagicEffect(posit, CONST_ME_GREEN_RINGS)
                                addEvent(efekty, 250, {cid = cid, loc = pos(creatureFound.uid), s = 0})
                        end
                        end
                        end
                    else
                        if isSightClear(poss, pos(cid), true) then
                            if underItemFound_1.uid == 0 then
                                doTransformItem(bagno, bagno_walkable)
                                doCreateMonster("Swampy Bilith", poss, false)
                            else
                                return true
                            end
                                doSendMagicEffect(poss, CONST_ME_GREEN_RINGS)
                                doTransformItem(bagno, id)
                                local hape = math.random(getCreatureMaxHealth(cid)*0.1,getCreatureMaxHealth(cid)*0.2)
                                doSendDistanceShoot(poss, pos(cid), CONST_ANI_EARTH)
                                doSendMagicEffect(pos(cid), CONST_ME_SMALLPLANTS)
                                doCreatureAddHealth(cid, hape)
                                doSendAnimatedText(pos(cid), hape, COLOR_LIGHTGREEN)
                        end
                    end
                end
        end
    end
end
    return true
end  