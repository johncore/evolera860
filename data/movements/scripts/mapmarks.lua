local config = {
	storage = 9432,
	version = 1, -- Increase this value after adding new marks, so player can step again and receive new map marks
	marks = {
		{mark = 5, pos = {x = 1005, y = 999, z = 7}, desc = "Evolera Temple"},
		{mark = 7, pos = {x = 1040, y = 1002, z = 7}, desc = "Depo!"},
		{mark = 7, pos = {x = 1043, y = 1006, z = 7}, desc = "Depo!"},
		{mark = 7, pos = {x = 1028, y = 983, z = 7}, desc = "NPC with tools!"},
		{mark = 2, pos = {x = 1007, y = 1037, z = 7}, desc = "South Gate!"},
		{mark = 2, pos = {x = 1078, y = 1006, z = 7}, desc = "Zamek!"},
		{mark = 2, pos = {x = 958, y = 1026, z = 7}, desc = "West Gate!"},
		{mark = 3, pos = {x = 941, y = 978, z = 7}, desc = "Statek!"},
		{mark = 3, pos = {x = 959, y = 948, z = 7}, desc = "Nowe Domki !"},
		{mark = 3, pos = {x = 1086, y = 978, z = 7}, desc = "Guild House !"},
		{mark = 1, pos = {x = 666, y = 666, z = 6}}
	}
}

local f_addMark = doPlayerAddMapMark
if(not f_addMark) then f_addMark = doAddMapMark end

function onStepIn(cid, item, position, fromPosition)
	if(isPlayer(cid) ~= TRUE or getPlayerStorageValue(cid, config.storage) == config.version) then
		return
	end

	for _, m  in pairs(config.marks) do
		f_addMark(cid, m.pos, m.mark, m.desc ~= nil and m.desc or "")
	end
	setPlayerStorageValue(cid, config.storage, config.version)
	return TRUE
end