local c = {cost = 100000, position = {x = 1199, y = 696, z = 10}, effect = 10}
function onStepIn(cid, item, position, fromPosition)
    if doPlayerRemoveMoney(cid, c.cost) then
        doTeleportThing(cid, c.position, true)
        doSendMagicEffect(c.position, c.effect)
    else
        doPlayerSendCancel(cid, "To Enter in Hardcore Quest you need to have " .. c.cost .. " gp.")
    end
    return true
end  