function onStepIn(cid, item, position, fromPosition)
	local config = {
		level = 30,
		shrines = {
			[7704] = {p = {x=1124, y=1035, z=7}, v = {2, 6}, m = "druid"},
			[7705] = {p = {x=1082, y=1079, z=8}, v = {2, 6}, m = "druid"},
			[7706] = {p = {x=1132, y=948, z=10}, v = {1, 5}, m = "sorcerer"},
			[7707] = {p = {x=1285, y=880, z=12}, v = {1, 5}, m = "sorcerer"},
			[7708] = {p = {x=1000, y=1000, z=7}, v = getPlayerVocation(cid)}
		}
	}
	if(config.shrines[item.actionid]) then
		if(isInArray(config.shrines[item.actionid].v, getPlayerVocation(cid)) == false or getPlayerLevel(cid) < config.level or isPremium(cid) == false) then
			doPlayerSendCancel(cid, "Only premium " .. config.shrines[item.actionid].m .. "s of level " .. config.level .. " or higher are able to enter this portal.")
			doTeleportThing(cid, fromPosition, true)
			return true
		end
		
		doTeleportThing(cid, config.shrines[item.actionid].p)
		doSendMagicEffect(config.shrines[item.actionid].p, CONST_ME_TELEPORT)
		doSendMagicEffect(toPosition, CONST_ME_POFF)
	end
	return true
end