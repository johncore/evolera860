questList = {
		--black knights, boots of haste
		{uniqueId = 7710, itemReward = 2195, experience = 4000, neededLevel = 0},
		--rotworms, two handed sword
		{uniqueId = 7711, itemReward = 2377, experience = 1000, neededLevel = 0},
		--rotworms, beholder shield
		{uniqueId = 7712, itemReward = 2518, experience = 1000, neededLevel = 0},
		--pits of inferno, soft boots
		{uniqueId = 7713, itemReward = 2640, experience = 0, neededLevel = 0},
		--pits of inferno, stuffed dragon
		{uniqueId = 7714, itemReward = 5791, experience = 0, neededLevel = 0},
		--pits of inferno, frozen starlight
		{uniqueId = 7715, itemReward = 2361, experience = 0, neededLevel = 0},
		--pits of inferno, ceremonial ankh
		{uniqueId = 7716, itemReward = 6561, experience = 0, neededLevel = 0},
		--behemothes, steel boots
		{uniqueId = 7717, itemReward = 2645, experience = 4000, neededLevel = 0},
		--behemothes, giant sword
		{uniqueId = 7718, itemReward = 2393, experience = 4000, neededLevel = 0},
		--outlaws, griffin shield
		{uniqueId = 7719, itemReward = 2533, experience = 1500, neededLevel = 0},
		--dragons, ornamented shield
		{uniqueId = 7720, itemReward = 2524, experience = 2000, neededLevel = 0},
		--demons, demon shield
		{uniqueId = 7721, itemReward = 2520, experience = 10000, neededLevel = 0},
		--demons, golden legs
		{uniqueId = 7722, itemReward = 2470, experience = 10000, neededLevel = 0},
		--heroes, crown shield
		{uniqueId = 7723, itemReward = 2519, experience = 2500, neededLevel = 0},
		--warlocks, mastermind shield
		{uniqueId = 7724, itemReward = 2514, experience = 10000, neededLevel = 0},
		--dragons, dragon scale mail
		{uniqueId = 7725, itemReward = 2492, experience = 2500, neededLevel = 50},
		--dragons, dragon lance
		{uniqueId = 7726, itemReward = 2414, experience = 2000, neededLevel = 50},
		--heroes, crown helmet
		{uniqueId = 7727, itemReward = 2491, experience = 3000, neededLevel = 0},
		--heroes, crown legs
		{uniqueId = 7728, itemReward = 2488, experience = 3000, neededLevel = 0},
		--heroes, crown armor
		{uniqueId = 7729, itemReward = 2487, experience = 3000, neededLevel = 0},
		--necromancers, devil helmet
		{uniqueId = 7730, itemReward = 2462, experience = 1000, neededLevel = 0},
		--necromancers, knight armor
		{uniqueId = 7731, itemReward = 2476, experience = 2000, neededLevel = 0},
		--necromancers, knight legs
		{uniqueId = 7732, itemReward = 2477, experience = 2000, neededLevel = 0},
		--black knights, golden armor
		{uniqueId = 7733, itemReward = 2466, experience = 5000, neededLevel = 0},
		--quaras, mermaid comb
		{uniqueId = 7734, itemReward = 5945, experience = 2000, neededLevel = 0},
		--giant spider, dragon hammer
		{uniqueId = 7735, itemReward = 2434, experience = 1500, neededLevel = 0},
		--giant spider, dwarven axe
		{uniqueId = 7736, itemReward = 2435, experience = 2000, neededLevel = 0},
		--dragons, fire sword
		{uniqueId = 7737, itemReward = 2392, experience = 950, neededLevel = 0},
		--trolls, battle axe
		{uniqueId = 7738, itemReward = 2378, experience = 800, neededLevel = 0},
		--demon helmet quest, demon shield
		{uniqueId = 7739, itemReward = 2520, experience = 10000, neededLevel = 0},
		--demon helmet quest, demon helmet
		{uniqueId = 7740, itemReward = 2493, experience = 0, neededLevel = 0},
		--demon helmet quest, steel boots
		{uniqueId = 7741, itemReward = 2645, experience = 10000, neededLevel = 0}
	}