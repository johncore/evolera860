
local stones = {
        	[1] = {name="Earth Stone"},
        	[2] = {name="Icy Stone"},
        	[3] = {name="Fire Stone"},
        	[4] = {name="Wind Stone"},
	}

local pos = {
		[1] = {pos={x=1030, y=1054, z=7}},
        	[2] = {pos={x=1009, y=1055, z=7}},
        	[3] = {pos={x=999, y=1078, z=7}},
        	[4] = {pos={x=959, y=1065, z=7}},
	}



function onSay()
    local monst = stones[math.random(1, #stones)]
	local poss = pos[math.random(1, #pos)]
	print(monst, poss)
    if not(monst) then return TRUE end
    doCreateMonster(monst.name,poss.pos)
    doBroadcastMessage('[Event Stones]\n '.. monst.name ..' have been spawn. Find and defeat it!', 22)
    return TRUE
end  