function onSay(cid, words, param)
	local cities = {
		["temple"] = {x=1005, y=999, z=7},
		["carlin"] = {x=164, y=216, z=6},
		["orshabaal"] = {x=264, y=941, z=8}
		-- example
		-- ["NAME OF CITY"] = {x=POSITION X, y=POSITION Y, z=POSITION Z} 		
	}
	local access = 3 -- ACCESS CAN USING TELEPORT
	local city = cities[string.lower(param)]
		if city then
				if(getPlayerAccess(cid) >= access)then
					doSendMagicEffect(getPlayerPosition(cid), 2)
					doTeleportThing(cid, city)
					doPlayerSendTextMessage(cid, 20, "Welcome to the "..param)
					doSendMagicEffect(city, 10)
					return 1
				else
					doPlayerSendCancel(cid, "You cannot execute this command!")
					return 0
				end
		else
			doPlayerSendCancel(cid, "City Not Found!")
			return 0
		end

