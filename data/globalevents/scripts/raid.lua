local storage = 1344
local raids = {
	"Barbarian",
	"Demodras",
	"Dryads",
	"Elfs",
	"Fernfang",
	"Ferumbras",
	"Ghazbaran",
	"grynch clan goblin",
	"Halloweenhare",
	"Hornedfox",
	"Morgaroth",
	"Necropharus",
	"Nomad",
	"OrcsThais",
	"Orshabaal",
	"Pirates",
	"Quara",
	"Rats",
	"Scarabs",
	"The Old Widow",
	"Tiquandas Revenge",
	"Undead Jester",
	"UndeadDarashia",
	"UndeadArmy",
	"Wolfsraid",
	"Vampire",
	"Santa",
	"Lord of Hell"
}

function onThink(interval, lastExecution, thinkInterval)
	if getGlobalStorageValue(storage) == -1 or getGlobalStorageValue(storage) < os.time() then
		executeRaid(raids[math.random(1, #raids)])
		setGlobalStorageValue(storage, os.time() + 9 * 60 * 60)
	end
	return TRUE
end