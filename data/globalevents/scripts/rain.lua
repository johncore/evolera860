local rain = Rain:new()
 
function onThink(interval, lastExecution)
    local minX = 985
    local minY = 1012
    local maxX = 996
    local maxY = 1022
 
    local frompos = {x=math.random(minX, maxX), y=math.random(minY, maxY), z=7}
    local topos = {x=math.random(frompos.x, maxX), y=math.random(frompos.y, maxY), z=7}
 
    local effects = {
        snow = {
            disteffect = CONST_ANI_SNOWBALL,
            effect = CONST_ME_ENERGYAREA
        },
        rain = {
            disteffect = CONST_ANI_FIRE,
            effect = 6
 
        }
    }
    random = math.random(0, 10)
    if (random == 0) then
        rain.chance = math.random(10,30)
        rain:start({fromPos = frompos, toPos = topos}, effects.snow, 300, math.random(50, 60))
    else
        rain.chance = math.random(30,100)
        rain.createItem = {chance = math.random(0,10), item = {itemid = 1489, type = 1}}
        rain:start({fromPos = frompos, toPos = topos}, effects.rain, math.random(50, 100), math.random(60, 100))
    end
    return TRUE
end