local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

function onCreatureAppear(cid)            npcHandler:onCreatureAppear(cid)        end
function onCreatureDisappear(cid)        npcHandler:onCreatureDisappear(cid)        end
function onCreatureSay(cid, type, msg)        npcHandler:onCreatureSay(cid, type, msg)    end
function onThink()                npcHandler:onThink()                end

-- by qento --

function potion(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,200) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,7488,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function addon(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,150) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,9693,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end	

function lottery(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,80) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,5911,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function frag(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,100) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,9969,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function koshei(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,90) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8266,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function king(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,170) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,2644,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function soft(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,50) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,6132,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function firewalker(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,80) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,9933,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function slingshot(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,100) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,5907,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function master(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,80) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8888,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function goldeneye(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,50) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,2504,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function oceanborn(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,180) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8884,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function blessed(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,120) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,2523,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function flame(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,150) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8931,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function solar(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,150) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8925,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function cranial(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,130) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,7415,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function star(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,70) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,7735,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function magical(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,150) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,2184,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function spellbook(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,90) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8918,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function yalahari(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,50) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,9778,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function ethno(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,70) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8892,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function robe(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,100) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8890,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function dark(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,100) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8865,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

-- by qento --

keywordHandler:addKeyword({'help'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = "I sell Donation Items: super potion,addon doll,lottery box,frag remover,koshei amulet,king boots,soft boots,firewalker boots,sling shoot,master archers armor,goldeneye Legs,oceanborn leviathan armor,blessed shield,flame blade,solar axe,cranial basher,star wand,magical wand,spellbook of dark mysteries,yalahari mask,ethno coat,robe of the underworld,dark lords cape."})

local node1 = keywordHandler:addKeyword({'potion'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy a super potion for 200 premium points?'})
    node1:addChildKeyword({'yes'}, potion, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node1:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node2 = keywordHandler:addKeyword({'addon'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy addon doll for 150 premium points?'})
    node2:addChildKeyword({'yes'}, addon, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node2:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node3 = keywordHandler:addKeyword({'lottery'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy lottery box for 80 premium points?'})
    node3:addChildKeyword({'yes'}, lottery, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node3:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node4 = keywordHandler:addKeyword({'frag'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy frag remover for 100 premium points?'})
    node4:addChildKeyword({'yes'}, frag, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node4:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node5 = keywordHandler:addKeyword({'koshei'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy koshei amulet for 90 premium points?'})
    node5:addChildKeyword({'yes'}, koshei, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node5:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node6 = keywordHandler:addKeyword({'king'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy king boots for 170 premium points?'})
    node6:addChildKeyword({'yes'}, king, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node6:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node7 = keywordHandler:addKeyword({'soft'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy soft boots for 50 premium points?'})
    node7:addChildKeyword({'yes'}, soft, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node7:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node8 = keywordHandler:addKeyword({'firewalker'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy firewalker for 80 premium points?'})
    node8:addChildKeyword({'yes'}, firewalker, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node8:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node9 = keywordHandler:addKeyword({'slingshot'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy sling shoot for 100 premium points?'})
    node9:addChildKeyword({'yes'}, slingshot, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node9:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node10 = keywordHandler:addKeyword({'master'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy frag remover for 80 premium points?'})
    node10:addChildKeyword({'yes'}, master, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node10:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node11 = keywordHandler:addKeyword({'goldeneye'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy koshei amulet for 50 premium points?'})
    node11:addChildKeyword({'yes'}, goldeneye, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node11:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node12 = keywordHandler:addKeyword({'oceanborn'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy oceanborn leviathan armor for 180 premium points?'})
    node12:addChildKeyword({'yes'}, oceanborn, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node12:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node13 = keywordHandler:addKeyword({'blessed'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy blessed shield for 120 premium points?'})
    node13:addChildKeyword({'yes'}, blessed, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node13:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node14 = keywordHandler:addKeyword({'flame'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy flame blade for 150 premium points?'})
    node14:addChildKeyword({'yes'}, flame, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node14:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node15 = keywordHandler:addKeyword({'solar'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy solar axe for 150 premium points?'})
    node15:addChildKeyword({'yes'}, solar, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node15:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node16 = keywordHandler:addKeyword({'cranial'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy cranial basher  for 130 premium points?'})
    node16:addChildKeyword({'yes'}, cranial, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node16:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node17 = keywordHandler:addKeyword({'star'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy star wand for 70 premium points?'})
    node17:addChildKeyword({'yes'}, star, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node17:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node18 = keywordHandler:addKeyword({'magical'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy magical wand for 150 premium points?'})
    node18:addChildKeyword({'yes'}, magical, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node18:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node19 = keywordHandler:addKeyword({'spellbook'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy spellbook of dark mysteries for 90 premium points?'})
    node19:addChildKeyword({'yes'}, spellbook, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node19:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node20 = keywordHandler:addKeyword({'yalahari'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy yalahari mask for 50 premium points?'})
    node20:addChildKeyword({'yes'}, yalahari, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node20:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node21 = keywordHandler:addKeyword({'ethno'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy ethno coat for 70 premium points?'})
    node21:addChildKeyword({'yes'}, ethno, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node21:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node22 = keywordHandler:addKeyword({'robe'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy robe of the underworld for 100 premium points?'})
    node22:addChildKeyword({'yes'}, robe, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node22:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node23 = keywordHandler:addKeyword({'dark'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy Dark lords cape for 100 premium points?'})
    node23:addChildKeyword({'yes'}, dark, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node23:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

npcHandler:addModule(FocusModule:new())