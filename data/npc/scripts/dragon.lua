local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}
function onCreatureAppear(cid) npcHandler:onCreatureAppear(cid) end
function onCreatureDisappear(cid) npcHandler:onCreatureDisappear(cid) end
function onCreatureSay(cid, type, msg) npcHandler:onCreatureSay(cid, type, msg) end
function onThink() npcHandler:onThink() end
function creatureSayCallback(cid, type, msg)
if(not npcHandler:isFocused(cid)) then
return false
end
local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
if(msgcontains(msg, 'adventures')) then
selfSay('Ok, you can started "first mission"?', cid)
end
---------------------------------------------------------
if(msgcontains(msg, 'first mission')) then
selfSay('Please bring me  "hydra egg"', cid)
talkState[talkUser] = 1
elseif(msgcontains(msg, 'hydra') and talkState[talkUser] == 1) then
if (getPlayerStorageValue(cid,100) > 0) then
selfSay('You finished this mission.', cid)
else
if(doPlayerRemoveItem(cid, 4850, 1) == TRUE) then
setPlayerStorageValue(cid,100,1)
doPlayerAddExperience(cid,10000)
selfSay('Thank you! You can started "second mission".. (you received 10000 points of experience)', cid)
else
selfSay('You must have more items', cid)
end
end
return true
end
---------------------------------------------------------
if(msgcontains(msg, 'second mission')) then
selfSay('Im hungry... Please bring me 100 "ham"', cid)
talkState[talkUser] = 1
elseif(msgcontains(msg, 'ham') and talkState[talkUser] == 1) then
if (getPlayerStorageValue(cid,101) > 0) then
selfSay('You finished this mission.', cid)
else
if(doPlayerRemoveItem(cid, 2671, 100) == TRUE) then
setPlayerStorageValue(cid,101,1)
doPlayerAddExperience(cid,30000)
selfSay('Thank you! You can started "third mission".. (you received 30000 points of experience)', cid)
else
selfSay('You must have more items', cid)
end
end
return true
end
---------------------------------------------------------
if(msgcontains(msg, 'third mission')) then
selfSay('Please bring me 50 "red dragon leathers"', cid)
talkState[talkUser] = 1
elseif(msgcontains(msg, 'red dragon leathers') and talkState[talkUser] == 1) then
if (getPlayerStorageValue(cid,102) > 0) then
selfSay('You finished this mission.', cid)
else
if(doPlayerRemoveItem(cid, 5948, 50) == TRUE) then
setPlayerStorageValue(cid,102,1)
doPlayerAddExperience(cid,50000)
selfSay('Thank you! You finished all missions. (you received 50000 points of experience)', cid)
else
selfSay('You must have more items', cid)
end
end
return true
end
---------------------------------------------------------
if(msgcontains(msg, 'fourth mission')) then
selfSay('Please bring me 10 "undead hearts"', cid)
talkState[talkUser] = 1
elseif(msgcontains(msg, 'heart') and talkState[talkUser] == 1) then
if (getPlayerStorageValue(cid,103) > 0) then
selfSay('You finished this mission.', cid)
else
if(doPlayerRemoveItem(cid, 11361, 10) == TRUE) then
setPlayerStorageValue(cid,103,1)
doPlayerAddExperience(cid,500000)
selfSay('Thank you! You finished all missions. (you received 500000 points of experience)', cid)
else
selfSay('You must have more items', cid)
end
end
return true
end
---------------------------------------------------------
if(msgcontains(msg, 'fifth mission')) then
selfSay('Please bring me 500 "fire mushrooms"', cid)
talkState[talkUser] = 1
elseif(msgcontains(msg, 'fire mushrooms') and talkState[talkUser] == 1) then
if (getPlayerStorageValue(cid,104) > 0) then
selfSay('You finished this mission.', cid)
else
if(doPlayerRemoveItem(cid, 2795, 500) == TRUE) then
setPlayerStorageValue(cid,104,1)
doPlayerAddExperience(cid,2500000)
selfSay('Thank you! You finished all missions. (you received 2500000 points of experience)', cid)
else
selfSay('You must have more items', cid)
end
end
return true
end
---------------------------------------------------------
if(msgcontains(msg, 'sixth mission')) then
selfSay('Please bring me 200 "demon dusts"', cid)
talkState[talkUser] = 1
elseif(msgcontains(msg, 'demon dusts') and talkState[talkUser] == 1) then
if (getPlayerStorageValue(cid,105) > 0) then
selfSay('You finished this mission.', cid)
else
if(doPlayerRemoveItem(cid, 5906, 200) == TRUE) then
setPlayerStorageValue(cid,105,1)
doPlayerAddExperience(cid,3000000)
selfSay('Thank you! You finished all missions. (you received 3000000 points of experience)', cid)
else
selfSay('You must have more items', cid)
end
end
return true
end

---------------------------------------------------------
if(msgcontains(msg, 'seventh mission')) then
selfSay('Please bring me 300 "crystal coins"', cid)
talkState[talkUser] = 1
elseif(msgcontains(msg, 'crystal coin') and talkState[talkUser] == 1) then
if (getPlayerStorageValue(cid,106) > 0) then
selfSay('You finished this mission.', cid)
else
if(doPlayerRemoveItem(cid, 2160, 300) == TRUE) then
setPlayerStorageValue(cid,106,1)
doPlayerAddExperience(cid,4000000)
selfSay('Thank you! You finished all missions. (you received 4000000 points of experience)', cid)
else
selfSay('You must have more items', cid)
end
end
return true
end---------------------------------------------------------
if(msgcontains(msg, 'eight mission')) then
selfSay('Please bring me 20 "demon legs"', cid)
talkState[talkUser] = 1
elseif(msgcontains(msg, 'demon legs') and talkState[talkUser] == 1) then
if (getPlayerStorageValue(cid,107) > 0) then
selfSay('You finished this mission.', cid)
else
if(doPlayerRemoveItem(cid, 2495, 20) == TRUE) then
setPlayerStorageValue(cid,107,1)
doPlayerAddExperience(cid,9000000)
selfSay('Thank you! You finished all missions. (you received 9000000 points of experience)', cid)
else
selfSay('You must have more items', cid)
end
end
return true
end
---------------------------------------------------------
if(msgcontains(msg, 'ninth mission')) then
selfSay('Please bring me 100 "marijuanas"', cid)
talkState[talkUser] = 1
elseif(msgcontains(msg, 'marijuanas') and talkState[talkUser] == 1) then
if (getPlayerStorageValue(cid,108) > 0) then
selfSay('You finished this mission.', cid)
else
if(doPlayerRemoveItem(cid, 5953, 100) == TRUE) then
setPlayerStorageValue(cid,108,1)
doPlayerAddExperience(cid,12000000)
doPlayerAddItem(cid,10523)
selfSay('Thank you! You finished all missions. (you received 12000000 points of experience)', cid)
else
selfSay('You must have more items', cid)
end
end
return true
end
---------------------------------------------------------
if(msgcontains(msg, 'final mission')) then
selfSay('Please bring me my egg "Sacred dragon egg"', cid)
talkState[talkUser] = 1
elseif(msgcontains(msg, 'Sacred dragon egg') and talkState[talkUser] == 1) then
if (getPlayerStorageValue(cid,109) > 0) then
selfSay('You finished this mission.', cid)
else
if(doPlayerRemoveItem(cid, 10523, 1) == TRUE) then
setPlayerStorageValue(cid,109,1)
doPlayerAddExperience(cid,25000000)
selfSay('Thank you! You finished all missions. Thank you ! Take my reward. (you received 25000000 points of experience)', cid)
else
selfSay('You must have more items', cid)
end
end
return true
end






end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())  