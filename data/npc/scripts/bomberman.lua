local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

function onCreatureAppear(cid)            npcHandler:onCreatureAppear(cid)        end
function onCreatureDisappear(cid)        npcHandler:onCreatureDisappear(cid)        end
function onCreatureSay(cid, type, msg)        npcHandler:onCreatureSay(cid, type, msg)    end
function onThink()                npcHandler:onThink()                end

-- by qento --

function falcon(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,8976) >= 1 then
        if doPlayerRemoveItem(cid,8976,1) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,5953,3)
        end
        else
            npcHandler:say('Nie masz falcona!', cid)
   end   
end

-- by qento --

keywordHandler:addKeyword({'help'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = "jezeli chcesz zamienic boomberman falcon na marijuanki wystarczy ze napiszesz 'falcon'"})

local node1 = keywordHandler:addKeyword({'falcon'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Chcesz zamienic bomber falcon na marijuanki?'})
    node1:addChildKeyword({'yes'}, falcon, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node1:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz bomberman falcona.', reset = true})

npcHandler:addModule(FocusModule:new())