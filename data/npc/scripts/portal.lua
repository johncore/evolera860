local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
local talkState = {}
function onCreatureAppear(cid)                npcHandler:onCreatureAppear(cid)            end
function onCreatureDisappear(cid)            npcHandler:onCreatureDisappear(cid)            end
function onCreatureSay(cid, type, msg)            npcHandler:onCreatureSay(cid, type, msg)        end
function onThink()                    npcHandler:onThink()                    end
function creatureSayCallback(cid, type, msg)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
    local talkUser = NPCHANDLER_CONVBEHAVIOR == CONVERSATION_DEFAULT and 0 or cid
local storage = 1
local x = getPlayerStorageValue(cid, storage)
local y = getPlayerStorageValue(cid, storage+1)
local z = getPlayerStorageValue(cid, storage+2)
    if(msgcontains(msg, 'teleport') or msgcontains(msg, 'teleportowac') or msgcontains(msg, 'tp')) then
        selfSay('Czy chcesz byc teleportowany do twojego ostatniego miejsca spoczynku za 10000 gold coins?', cid)
        talkState[talkUser] = 1
    elseif(msgcontains(msg, 'yes') or msgcontains(msg, 'tak')) and talkState[talkUser] == 1 then
    if(getPlayerStorageValue(cid, storage+3) > os.time()) then
    if(not getTileInfo({x=x, y=y, z=z}).nologout) then
    if(x > -1 and y > -1) then
            if(doPlayerRemoveMoney(cid, 100000) == TRUE) then
                doTeleportThing(cid, {x=x, y=y, z=z})
                doSendMagicEffect({x=x, y=y, z=z}, CONST_ME_TELEPORT)
            else
                selfSay('Przykro mi ale potrzebujesz 100000 gold coins aby byc teleportowany.', cid)
            end
                else
                selfSay('Jeszcze nie umarles ani razu.', cid)
            end
                else
                selfSay('Niestety ale nie mozesz byc teleportowany do tego miejsca.', cid)
            end
                else
                selfSay('Mozesz byc teleportowany do twojego ostatniego miejsca smierci tylko po czasie mniejszym niz 10 minut.', cid)
            end
    elseif(msgcontains(msg, 'no') and isInArray({1}, talkState[talkUser]) == TRUE) then
        talkState[talkUser] = 0
        selfSay('Ok then.', cid)
    end
    return true
end
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback)
npcHandler:addModule(FocusModule:new())