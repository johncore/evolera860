local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

function onCreatureAppear(cid)            npcHandler:onCreatureAppear(cid)        end
function onCreatureDisappear(cid)        npcHandler:onCreatureDisappear(cid)        end
function onCreatureSay(cid, type, msg)        npcHandler:onCreatureSay(cid, type, msg)    end
function onThink()                npcHandler:onThink()                end

-- by qento --

function super potion(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,200) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,7488,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function addon doll(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,150) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,9693,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end	

function lottery box(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,80) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,5911,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function frag remover(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,100) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,9969,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function koshei amulet(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,90) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8266,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function king boots(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,170) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,2644,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function soft boots(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,50) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,6132,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function firewalker boots(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,80) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,9933,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function sling shot(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,100) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,5907,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function master archers armor(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,80) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8888,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function goldeneye Legs(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,50) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,2504,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function oceanborn leviathan armor(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,180) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8884,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function blessed shield(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,120) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,2523,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function flame blade(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,150) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8931,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function solar axe(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,150) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8925,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function cranial basher(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,130) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,7415,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function star wand(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,70) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,7735,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function magical wand(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,150) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,2184,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function spellbook of dark mysteries(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,90) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8918,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function yalahari mask(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,50) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,9778,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function ethno coat(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,70) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8892,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function robe of the underworld(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,100) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8890,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

function dark lords cape(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,2157) >= 1 then
        if doPlayerRemoveItem(cid,2157,100) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8865,1)
        end
        else
            npcHandler:say('You don\'t have these items!', cid)
   end   
end

-- by qento --

keywordHandler:addKeyword({'help'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = "I sell Donation Items: super potion,addon doll,lottery box,frag remover,koshei amulet,king boots,soft boots,firewalker boots,sling shoot,master archers armor,goldeneye Legs,oceanborn leviathan armor,blessed shield,flame blade,solar axe,cranial basher,star wand,magical wand,spellbook of dark mysteries,yalahari mask,ethno coat,robe of the underworld,dark lords cape."})

local node1 = keywordHandler:addKeyword({'super potion'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy a star wand for 200 premium points?'})
    node1:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node1:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node2 = keywordHandler:addKeyword({'addon doll'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy addon doll for 150 premium points?'})
    node2:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node2:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node3 = keywordHandler:addKeyword({'lottery box'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy lottery box for 80 premium points?'})
    node3:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node3:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node4 = keywordHandler:addKeyword({'frag remover'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy frag remover for 100 premium points?'})
    node4:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node4:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node5 = keywordHandler:addKeyword({'koshei amulet'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy koshei amulet for 90 premium points?'})
    node5:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node5:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node6 = keywordHandler:addKeyword({'king boots'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy king boots for 170 premium points?'})
    node6:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node6:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node7 = keywordHandler:addKeyword({'soft boots'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy soft boots for 50 premium points?'})
    node7:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node7:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node8 = keywordHandler:addKeyword({'firewalker boots'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy FWB for 80 premium points?'})
    node8:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node8:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node9 = keywordHandler:addKeyword({'sling shot'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy sling shoot for 100 premium points?'})
    node9:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node9:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node10 = keywordHandler:addKeyword({'master archers armor'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy frag remover for 80 premium points?'})
    node10:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node10:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node11 = keywordHandler:addKeyword({'goldeneye Legs'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy koshei amulet for 50 premium points?'})
    node11:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node11:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node12 = keywordHandler:addKeyword({'oceanborn leviathan armor'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy oceanborn leviathan armor for 180 premium points?'})
    node12:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node12:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node13 = keywordHandler:addKeyword({'blessed shield'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy blessed shield for 120 premium points?'})
    node13:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node13:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node14 = keywordHandler:addKeyword({'flame blade'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy flame blade for 150 premium points?'})
    node14:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node14:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node15 = keywordHandler:addKeyword({'solar axe'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy solar axe for 150 premium points?'})
    node15:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node15:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node16 = keywordHandler:addKeyword({'cranial basher'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy cranial basher  for 130 premium points?'})
    node16:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node16:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node17 = keywordHandler:addKeyword({'star wand'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy star wand for 70 premium points?'})
    node17:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node17:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node18 = keywordHandler:addKeyword({'magical wand'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy magical wand for 150 premium points?'})
    node18:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node18:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node19 = keywordHandler:addKeyword({'spellbook of dark mysteries'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy spellbook of dark mysteries for 90 premium points?'})
    node19:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node19:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node20 = keywordHandler:addKeyword({'yalahari mask'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy yalahari mask for 50 premium points?'})
    node20:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node20:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node21 = keywordHandler:addKeyword({'ethno coat'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy ethno coat for 70 premium points?'})
    node21:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node21:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})
	
local node22 = keywordHandler:addKeyword({'robe of the underworld'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy robe of the underworld for 100 premium points?'})
    node22:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node22:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

local node23 = keywordHandler:addKeyword({'dark lords cape'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Do you want to buy Dark lords cape for 100 premium points?'})
    node23:addChildKeyword({'yes'}, amulet, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node23:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz premium coinsow...', reset = true})

npcHandler:addModule(FocusModule:new())