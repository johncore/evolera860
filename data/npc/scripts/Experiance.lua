local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)

function onCreatureAppear(cid)            npcHandler:onCreatureAppear(cid)        end
function onCreatureDisappear(cid)        npcHandler:onCreatureDisappear(cid)        end
function onCreatureSay(cid, type, msg)        npcHandler:onCreatureSay(cid, type, msg)    end
function onThink()                npcHandler:onThink()                end

-- by qento --

function marijuana(cid, message, keywords, parameters, node)
    if(not npcHandler:isFocused(cid)) then
        return false
    end
        if getPlayerItemCount(cid,5953) >= 1 then
        if doPlayerRemoveItem(cid,5953,50) then
            npcHandler:say('Here is your item!', cid)
            doPlayerAddItem(cid,8300,1)
        end
        else
            npcHandler:say('Nie masz ganji!', cid)
   end   
end

-- by qento --

keywordHandler:addKeyword({'help'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = "no co ty czy to trudne napisz marijuana i wymien je na experiance."})

local node1 = keywordHandler:addKeyword({'marijuana'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Chcesz zamienic 50 marijuanek na experiance crystal?'})
    node1:addChildKeyword({'yes'}, marijuana, {npcHandler = npcHandler, onlyFocus = true, reset = true})
    node1:addChildKeyword({'no'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Sory ziomus nie posiadasz 50 marijuanek', reset = true})

npcHandler:addModule(FocusModule:new())