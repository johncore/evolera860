local function doNPCTalkALot(cid, npc, delay, messages) 
    for i = 1, #messages do 
        addEvent(function() 
                        if isPlayer(cid) then 
                            doCreatureSay(npc, messages[i], TALKTYPE_PRIVATE_NP, false, cid) 
                        end end, 
                        (i > 1 and 5000*i+delay or delay), 
                        cid, messages, i) 
    end 
end 

local keywordHandler = KeywordHandler:new() 
local npcHandler = NpcHandler:new(keywordHandler) 
NpcSystem.parseParameters(npcHandler) 
local talkState = {} 
function onCreatureAppear(cid)                npcHandler:onCreatureAppear(cid)            end 
function onCreatureDisappear(cid)            npcHandler:onCreatureDisappear(cid)            end 
function onCreatureSay(cid, type, msg)            npcHandler:onCreatureSay(cid, type, msg)        end 
function onThink()                    npcHandler:onThink()                    end 

local main_storage, bosses_storage = 10001, 10002 
local good_hunting = {'Happy hunting, old chap! Come back to me when you are through with your {task}.', 'Good show, old chap! Speak to me again when you are done hunting.'}     
local good_job = {'That took some time, huh? Good hunting though. If you\'re up for another hunting mission just ask me for a {task}.', 'If you\'re up for another hunting mission just ask me for a {task}.'} 
local tasks = { 
            ['trolls'] = {min_level = 8, max_level = 20, 
                    rewards = {'money', 0, 200, 'exp', 0, 200}, 
                    begin_storage_value = 1, finish_storage_value = 2, boss = false, talkstate = 1}, 
            ['rotworms'] = {min_level = 15, max_level = 30, 
                    rewards = {'money', 0, 400, 'exp', 0, 1000}, 
                    begin_storage_value = 5, finish_storage_value = 6, boss = true, before_kill_boss = 1, after_kill_boss = 2, talkstate = 3}, 
            ['cyclops'] = {min_level = 18, max_level = 50, 
                    rewards = {'money', 0, 800, 'exp', 0, 3000, 'item', 2381, 1}, 
                    begin_storage_value = 7, finish_storage_value = 8, boss = false, talkstate = 4}, 
            ['dragons'] = {min_level = 40, max_level = 180,  
                    rewards = {'item', 2516, 1, 'exp', 0, 10000, 'money', 0, 5000}, 
                    begin_storage_value = 33, finish_storage_value = 34, boss = true, before_kill_boss = 7, after_kill_boss = 8, talkstate = 27},  
	        ['dragon lords'] = {min_level = 40, max_level = 500,  
                    rewards = {'item', 2160, 100, 'exp', 0, 10000, 'money', 0, 5000}, 
                    begin_storage_value = 33, finish_storage_value = 34, boss = true, before_kill_boss = 3, after_kill_boss = 4, talkstate = 28}, 
		    ['wyrms'] = {min_level = 40, max_level = 500,  
                    rewards = {'item', 2160, 200, 'exp', 0, 10000, 'money', 0, 6000}, 
                    begin_storage_value = 33, finish_storage_value = 34, boss = true, before_kill_boss = 3, after_kill_boss = 4, talkstate = 28}, 
		    ['nightmare'] = {min_level = 40, max_level = 500,  
                    rewards = {'item', 2160, 300, 'exp', 0, 10000, 'money', 0, 9000}, 
                    begin_storage_value = 33, finish_storage_value = 34, boss = true, before_kill_boss = 3, after_kill_boss = 4, talkstate = 28}, 
            ['hydra'] = {min_level = 90, max_level = 250, 
                    rewards = {'exp', 0, 15000, 'addon', {144,148}, 1}, 
                    begin_storage_value = 51, finish_storage_value = 52, boss = false, talkstate = 16}, 
            ['behemoth'] = {min_level = 90, max_level = 250, 
                    rewards = {'money', 0, 5000, 'addon', {143,147}, 2, 'skill', SKILL_AXE, 1}, 
                    begin_storage_value = 53, finish_storage_value = 54, boss = true, before_kill_boss = 11, after_kill_boss = 12, talkstate = 18}, 
            ['serpent spawn'] = {min_level = 90, max_level = 250, 
                    rewards = {'money', 0, 15000, 'addon', {144,148}, 2}, 
                    begin_storage_value = 55, finish_storage_value = 56, boss = false, talkstate = 19}, 
            ['demon'] = {min_level = 100, max_level = 300, 
                    rewards = {'item', 2128, 75, 'addon', {288,289}, 2, 'item', 10518, 1}, 
                    begin_storage_value = 57, finish_storage_value = 58, boss = true, before_kill_boss = 13, after_kill_boss = 14, talkstate = 25}, 
            } 

local function doTakeReward(cid, table) 
for i = 1, #table/3 do 
    if table[i*3-2] == 'item' then 
        doPlayerAddItem(cid, table[i*3-1], table[i*3], true) 
    elseif table[i*3-2] == 'exp' then 
        doAddExp(cid, table[i*3]) 
    elseif table[i*3-2] == 'storage' then 
        doPlayerSetStorageValue(cid, table[i*3-1], table[i*3]) 
    elseif table[i*3-2] == 'money' then 
        doPlayerAddMoney(cid, table[i*3]) 
    elseif table[i*3-2] == 'skill' then 
        doPlayerAddSkill(cid, table[i*3-1], table[i*3]) 
    elseif table[i*3-2] == 'addon' then 
        doPlayerAddOutfit(cid, (getPlayerSex(cid) == 0 and table[i*3-1][1] or table[i*3-1][2]), table[i*3]) 
    end 
end 
return true 
end 

function creatureSayCallback(cid, type, msg) 
s = 'All right, what would you like to hunt - ' 
for k, v in pairs(tasks) do 
    s = s .. (getPlayerLevel(cid) >= v.min_level and getPlayerLevel(cid) <= v.max_level and '{'..k..',} ' or '') 
end 
s = string.sub(s, 1, s:len() - 3)..'}.' 
if isInArray({'task','mission','quest'}, msg:lower()) then 
    if getPlayerStorageValue(cid, main_storage) <= 0 then 
        talkState[cid] = 100 
        return doNPCTalkALot(cid, getNpcCid(), 200, s) 
    else 
        for k, v in pairs(tasks) do 
            if getPlayerStorageValue(cid, main_storage) == v.finish_storage_value and getPlayerStorageValue(cid, bosses_storage) < v.before_kill_boss then 
                if v.boss then 
                    doPlayerSetStorageValue(cid, bosses_storage, v.before_kill_boss) 
                    return doNPCTalkALot(cid, getNpcCid(), 200, {'Good job, go kill boss of '..k..'.'}) 
                else  
                    doPlayerSetStorageValue(cid, bosses_storage, 0) 
                    doPlayerSetStorageValue(cid, main_storage, 0) 
                    doTakeReward(cid, v.rewards) 
                    return doNPCTalkALot(cid, getNpcCid(), 200, {good_job[math.random(#good_job)]}) 
                end 
            elseif getPlayerStorageValue(cid, bosses_storage) == v.after_kill_boss then 
                doPlayerSetStorageValue(cid, bosses_storage, -1) 
                doPlayerSetStorageValue(cid, main_storage, -1) 
                doTakeReward(cid, v.rewards) 
                return doNPCTalkALot(cid, getNpcCid(), 200, {good_job[math.random(#good_job)]}) 
            elseif getPlayerStorageValue(cid, bosses_storage) == v.before_kill_boss then 
                return doNPCTalkALot(cid, getNpcCid(), 200, {'You do not kill boss yet.'}) 
            end 
        end 
    end 
elseif isInArray({'cancel','abort','stop'}, msg:lower()) then 
    if getPlayerStorageValue(cid, main_storage) > 0 then 
        talkState[cid] = 101 
        return doNPCTalkALot(cid, getNpcCid(), 200, {'Do you want cancel actual mission?'}) 
    else 
        return doNPCTalkALot(cid, getNpcCid(), 200, {'You don\'t have started any task!'}) 
    end 
end 
for k, v in pairs(tasks) do 
    if isInArray({'yes','ok','yep'}, msg:lower()) then 
        if talkState[cid] == v.talkstate then 
            doPlayerSetStorageValue(cid, main_storage, v.begin_storage_value) 
            return doNPCTalkALot(cid, getNpcCid(), 5000, {good_hunting[math.random(#good_hunting)]}) 
        elseif talkState[cid] == 101 and getPlayerStorageValue(cid, main_storage) > 0 then 
            doPlayerSetStorageValue(cid, main_storage, -1) 
            return doNPCTalkALot(cid, getNpcCid(), 5000, {'Speak to me again when you are done hunting.'}) 
        end 
    elseif isInArray({'no','never','nope'}, msg:lower()) and talkState[cid] > 0 then 
        return doNPCTalkALot(cid, getNpcCid(), 5000, {'Ok then.'}) 
    elseif isInArray({k,string.sub(k, 1, k:len() - 1)}, msg:lower()) and talkState[cid] == 100 then 
        if getPlayerLevel(cid) >= v.min_level and getPlayerLevel(cid) <= v.max_level then 
            talkState[cid] = v.talkstate 
            return doNPCTalkALot(cid, getNpcCid(), 5000, {'Do you want hunt '..k..'?'}) 
        else 
            return doNPCTalkALot(cid, getNpcCid(), 5000, {'You do not have enough level.'}) 
        end 
    end 
end 
return true 
end 
npcHandler:setCallback(CALLBACK_MESSAGE_DEFAULT, creatureSayCallback) 
npcHandler:addModule(FocusModule:new())  