-- NPC BY BANCO --
local keywordHandler = KeywordHandler:new()
local npcHandler = NpcHandler:new(keywordHandler)
NpcSystem.parseParameters(npcHandler)
function onCreatureAppear(cid)			npcHandler:onCreatureAppear(cid)			end
function onCreatureDisappear(cid)		npcHandler:onCreatureDisappear(cid)			end
function onCreatureSay(cid, type, msg)	npcHandler:onCreatureSay(cid, type, msg)	end
function onThink()						npcHandler:onThink()						end
local shopModule = ShopModule:new()
npcHandler:addModule(shopModule)
local node1 = keywordHandler:addKeyword({'rings'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'Na stanie mam takie ringi jak : Life Ring, Ring of Healing, Stealth Ring, Sword,Club,Axe Ring, Might Ring, Time Ring'})
shopModule:addBuyableItem({'life ring', 'life ring'},		        2168, 1500,		'life ring')
shopModule:addBuyableItem({'ring of healing', 'ring of healing'},	2214, 2000,		'ring of healing')
shopModule:addBuyableItem({'stealth ring', 'stealth ring'},			2165, 1000,		'stealth ring')
shopModule:addBuyableItem({'time ring', 'time ring'},				2169, 1000,		'time ring')
shopModule:addBuyableItem({'might ring', 'might ring'},				2164, 1500,		'might ring')
shopModule:addBuyableItem({'club ring', 'club ring'},				2209, 300,		'club ring')
shopModule:addBuyableItem({'sword ring', 'sword ring'},				2207, 300,		'sword ring')
shopModule:addBuyableItem({'axe ring', 'axe ring'},					2208, 300,		'axe ring')
local node2 = keywordHandler:addKeyword({'amulets'}, StdModule.say, {npcHandler = npcHandler, onlyFocus = true, text = 'I have Scarf, Platinum Amulet and Amulet of Loss.'})
shopModule:addBuyableItem({'platinum amulet', 'platinum amulet'},					2171, 10000,		'platinum amulet')
shopModule:addBuyableItem({'scarf', 'scarf'},						2661, 100,		'scarf')
shopModule:addBuyableItem({'amulet of loss', 'aol'},					2173, 20000,		'amulet of loss')
npcHandler:addModule(FocusModule:new())
